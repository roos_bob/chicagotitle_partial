function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

String.prototype.ToNumber = function ToNumber(){
    return Number(this.replace(/[$,]+/g,""));
};

String.prototype.ToDate = function ToDate(){
    return isNaN(Date.parse(this)) ? 0 : Date.parse(this);
};

Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});




