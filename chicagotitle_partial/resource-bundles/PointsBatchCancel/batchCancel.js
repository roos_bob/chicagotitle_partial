$j = jQuery.noConflict();
var inputData;
var bp = {}; 

$j(document).ready(function($) {
	
	$j("#sessionIdDiv").hide();
    $j("#popupLoading").show();
	$j('#submitCancel').click(startBatch);
    $j('#closingDatePicker').val(new Date().toDateInputValue());    
    $j('#inputText').on("input selectionchange propertychange", function(){
            $j("#popupLoading").show();
			var txt = $j('#inputText').val();
			inputData = Papa.parse(txt, {delimiter: "",newline: ""});
			//console.log("parse results: " , inputData);
            hideShowSubmitButton()
			$j("#popupLoading").hide();

    });
    $j('#closingDatePicker').on("input selectionchange propertychange", function(){
		hideShowSubmitButton();
    });
    $j('#comments').on("input selectionchange propertychange", function(){
		hideShowSubmitButton();
    });
    hideShowSubmitButton();
    $j("#popupLoading").hide();

});


function hideShowSubmitButton() {
	
	if ($j('#inputText').val()  && $j("select[id$='developerSelectList']").val()   && $j('#closingDatePicker').val() ) {
		$j("#submitCancel").prop('disabled', false);    
	} else {
		$j("#submitCancel").prop('disabled', true);
	}
	//console.log($j('[id$="sessionId"]').text());
}

function startBatch(){
	try {
		$j("#submitCancel").prop('disabled', true);
		sforce.connection.sessionId = $j('[id$="sessionId"]').text();
		var res = sforce.apex.execute(
		"PointsBatchCancelService",
		"cancelContracts",
		{ 
			contractNames : inputData.data,
			developerId: $j("select[id$='developerSelectList']").val(),
			closingDate: new Date($j('#closingDatePicker').val()), 
			comments: $j('#comments').val() }
		); 
	}
	catch(e) { 
		alert(e); 
		$j("#submitCancel").prop('disabled', false);
		return false;
	}
	alert("Contract Cancel job is enqueued. You'll receive an email once it's done.");
	return false;
}

