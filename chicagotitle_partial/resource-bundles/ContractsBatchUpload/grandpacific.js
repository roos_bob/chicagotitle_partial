function GrandPacificParse() {
    var that = new BatchParse; 

    that.parseData = function() {
        $j("#popupLoading").show();

        DEBUG && console.log('GrandPacificParse', 'parseData')
        that.addHeaders();
        that.addRecords();
        that.getValidationData(); //Will call a Remoting function that will in turn call validateRecords() and displayRecords();
        DEBUG && console.log("Synchronous parse results:", inputData);
    }
	/*
		function to run immediately after text is pasted into the input textbox
		If no prompts are needed, this can call the full parse function
	*/
    that.quickParse = function() {

        $j("#closingDate").show();
        $j("#expirationDate").hide();
        $j("#submit-parse").show();

        $j("#popupLoading").hide();
    }    
	/*
		Define column headers
	*/
    that.addHeaders = function() {
        var i = 1;
        var columns = [];
        columns.push( {name: "Problems", label: "Problems", inputColumn: 0, visible: true} );
        columns.push( {name: "Verified", label: "Verified", inputColumn: 0, visible: true} );
		columns.push( {name: "RowNumber", label: "Row Number", inputColumn: 0, visible: true} );

		columns.push( {name: "BatchNumber", label: "Batch", inputColumn: i++, visible: true} );
		columns.push( {name: "ResortName", label: "ResortName", inputColumn: i++, visible: true} );
		columns.push( {name: "Contract", label: "Contract", inputColumn: i++, visible: true} );
		columns.push( {name: "ContractId", label: "ContractId", inputColumn: i++, visible: true} );
		columns.push( {name: "SalesPerson", label: "SalesPerson", inputColumn: i++, visible: true} );
		columns.push( {name: "TO", label: "TO", inputColumn: i++, visible: true} );
		columns.push( {name: "QAM", label: "QAM", inputColumn: i++, visible: true} );
		columns.push( {name: "CustomerName1", label: "CustomerName1", inputColumn: i++, visible: true} );
		columns.push( {name: "CustomerName2", label: "CustomerName2", inputColumn: i++, visible: true} );
		columns.push( {name: "Source", label: "Source", inputColumn: i++, visible: true} );
		columns.push( {name: "Lender", label: "Lender", inputColumn: i++, visible: true} );
		columns.push( {name: "ContractPrice", label: "ContractPrice", inputColumn: i++, visible: true} );
		columns.push( {name: "SalesDate", label: "SalesDate", inputColumn: i++, visible: true} );
		columns.push( {name: "Financed", label: "Financed", inputColumn: i++, visible: true} );
		columns.push( {name: "PreviousLoanTransfer", label: "PreviousLoanTransfer", inputColumn: i++, visible: true} );
		columns.push( {name: "FundedAmount", label: "FundedAmount", inputColumn: i++, visible: true} );
		columns.push( {name: "TransferEquity", label: "TransferEquity", inputColumn: i++, visible: true} );
		columns.push( {name: "DownPayment", label: "DownPayment", inputColumn: i++, visible: true} );
		columns.push( {name: "PercentOfDP", label: "PercentOfDP", inputColumn: i++, visible: true} );
		columns.push( {name: "ClosingCost", label: "ClosingCost", inputColumn: i++, visible: true} );
		columns.push( {name: "FICO", label: "FICO", inputColumn: i++, visible: true} );
		columns.push( {name: "Interval", label: "Interval", inputColumn: i++, visible: true} );
		columns.push( {name: "Term", label: "Term", inputColumn: i++, visible: true} );
		columns.push( {name: "APR", label: "APR", inputColumn: i++, visible: true} );
		columns.push( {name: "PayAmount", label: "PayAmount", inputColumn: i++, visible: true} );
		columns.push( {name: "FirstPayDate", label: "FirstPayDate", inputColumn: i++, visible: true} );
		columns.push( {name: "NetCommissionSalesPrice", label: "NetCommissionSalesPrice", inputColumn: i++, visible: true} );
		columns.push( {name: "SalesCenter", label: "SalesCenter", inputColumn: i++, visible: true} );
		columns.push( {name: "FdiPointsProvided", label: "FdiPointsProvided", inputColumn: i++, visible: true} );
		columns.push( {name: "tu_unit_number1", label: "tu_unit_number1", inputColumn: i++, visible: true} );
		columns.push( {name: "wk_number1", label: "wk_number1", inputColumn: i++, visible: true} );
		columns.push( {name: "Usage", label: "Usage", inputColumn: i++, visible: true} );
		columns.push( {name: "inventory_usage_type1", label: "inventory_usage_type1", inputColumn: i++, visible: true} );
		columns.push( {name: "Ownership", label: "Ownership", inputColumn: i++, visible: true} );
		columns.push( {name: "CommissionDiscountAmount", label: "CommissionDiscountAmount", inputColumn: i++, visible: true} );
		columns.push( {name: "FdiPointsAuthorized", label: "FdiPointsAuthorized", inputColumn: i++, visible: true} );

        columns.push( {name:  "Name1Salutation", label: "Name 1 Salutation", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1First", label: "Name 1 First", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Middle", label: "Name 1 Middle", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Last", label: "Name 1 Last", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Suffix", label: "Name 1 Suffix", inputColumn: 0, visible: true} );

        columns.push( {name:  "Name2Salutation", label: "Name 2 Salutation", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2First", label: "Name 2 First", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Middle", label: "Name 2 Middle", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Last", label: "Name 2 Last", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Suffix", label: "Name 2 Suffix", inputColumn: 0, visible: true} );

		columns.push( {name: "DeveloperId", label: "Developer Id", inputColumn: 0, visible: true} );
		columns.push( {name: "ResortId", label: "Resort Id", inputColumn: 0, visible: true} );
		columns.push( {name: "UnitInventoryId", label: "Unit Inventory Id", inputColumn: 0, visible: true} );
		columns.push( {name: "UnitInventoryICN", label: "Unit Inventory ICN", inputColumn: 0, visible: true} );
		columns.push( {name: "UnitInventoryICNOccupied", label: "Unit Inventory Occupied", inputColumn: 0, visible: true} );
		columns.push( {name: "DeededContractId", label: "Deeded Contract Id", inputColumn: 0, visible: true} );
		columns.push( {name: "ContractCode", label: "Contract Code", inputColumn: 0, visible: true} );
		columns.push( {name: "ResortOwnershipCode", label: "Ownership Code", inputColumn: 0, visible: true} );
		columns.push( {name: "ResortOwnershipId", label: "Ownership Code Id", inputColumn: 0, visible: true} );

        inputData.headers = columns;
    }
	/*
		Add raw data to fields in each row
	*/
    that.addRecords = function() {

        var closingDate = $j("#closingDatePicker").val();
		var i = 1;
        inputData.records = [];
        inputData.data.forEach(function(item) {
            if (item.length < 36) {return;} // intended to remove a trailing new line row
            var rowData = {};
			var col = 0;
			rowData.RowNumber = {value: '' + i++, valid: true};
			rowData.isError = false;
			rowData.BatchNumber = {value: item[col++].trim(), valid: true};
			rowData.ResortName = {value: item[col++].trim(), valid: true};
			rowData.Contract = {value: item[col++].trim(), valid: true};
			rowData.ContractId = {value: item[col++].trim(), valid: true};
			rowData.SalesPerson = {value: item[col++].trim(), valid: true};
			rowData.TO = {value: item[col++].trim(), valid: true};
			rowData.QAM = {value: item[col++].trim(), valid: true};
			rowData.CustomerName1 = {value: item[col++].trim(), valid: true};
			rowData.CustomerName2 = {value: item[col++].trim(), valid: true};
			rowData.Source = {value: item[col++].trim(), valid: true};
			rowData.Lender = {value: item[col++].trim(), valid: true};
			rowData.ContractPrice = {value: item[col++].trim(), valid: true};
			rowData.SalesDate = {value: item[col++].trim(), valid: true};
			rowData.Financed = {value: item[col++].trim(), valid: true};
			rowData.PreviousLoanTransfer = {value: item[col++].trim(), valid: true};
			rowData.FundedAmount = {value: item[col++].trim(), valid: true};
			rowData.TransferEquity = {value: item[col++].trim(), valid: true};
			rowData.DownPayment = {value: item[col++].trim(), valid: true};
			rowData.PercentOfDP = {value: item[col++].trim(), valid: true};
			rowData.ClosingCost = {value: item[col++].trim(), valid: true};
			rowData.FICO = {value: item[col++].trim(), valid: true};
			rowData.Interval = {value: item[col++].trim(), valid: true};
			rowData.Term = {value: item[col++].trim(), valid: true};
			rowData.APR = {value: item[col++].trim(), valid: true};
			rowData.PayAmount = {value: item[col++].trim(), valid: true};
			rowData.FirstPayDate = {value: item[col++].trim(), valid: true};
			rowData.NetCommissionSalesPrice = {value: item[col++].trim(), valid: true};
			rowData.SalesCenter = {value: item[col++].trim(), valid: true};
			rowData.FdiPointsProvided = {value: item[col++].trim(), valid: true};
			rowData.tu_unit_number1 = {value: item[col++].trim(), valid: true};
			rowData.wk_number1 = {value: item[col++].trim(), valid: true};
			rowData.Usage = {value: item[col++].trim(), valid: true};
			rowData.inventory_usage_type1 = {value: item[col++].trim(), valid: true};
			rowData.Ownership = {value: item[col++].trim(), valid: true};
			rowData.CommissionDiscountAmount = {value: item[col++].trim(), valid: true};
			rowData.FdiPointsAuthorized = {value: item[col++].trim(), valid: true};

            rowData.Name1Salutation = {value: '', valid: true};
            rowData.Name1First = {value: '', valid: true};
            rowData.Name1Middle = {value: '', valid: true};
            rowData.Name1Last = {value: '', valid: true};
            rowData.Name1Suffix = {value: '', valid: true};
            rowData.Name2Salutation = {value: '', valid: true};
            rowData.Name2First = {value: '', valid: true};
            rowData.Name2Middle = {value: '', valid: true};
            rowData.Name2Last = {value: '', valid: true};
            rowData.Name2Suffix = {value: '', valid: true};

			rowData.DeveloperId = {value: '', valid: true};
			rowData.ResortId = {value: '', valid: true};
			rowData.UnitInventoryId = {value: '', valid: true};
			rowData.UnitInventoryICN = {value: '', valid: true};
			rowData.UnitInventoryICNOccupied = {value: '', valid: true};
			rowData.DeededContractId = {value: '', valid: true};
			rowData.ContractCode = {value: '', valid: true};
			rowData.ResortOwnershipCode = {value: '', valid: true};
			rowData.ResortOwnershipId = {value: '', valid: true};
			
            rowData.Problems = {value: '', valid: true};
            rowData.Verified = {value: 'false', valid: true};

            if (rowData.CustomerName1.value != '') {
                var n1 = parseFullName(rowData.CustomerName1.value);

                rowData.Name1Salutation = {value: n1.title, valid: true};
                rowData.Name1First = {value: n1.first, valid: true};
                rowData.Name1Middle = {value: n1.middle, valid: true};
                rowData.Name1Last = {value: n1.last, valid: true};
                rowData.Name1Suffix = {value: n1.suffix, valid: true};
            }

            if (rowData.CustomerName2.value != '') {
                var n2 = parseFullName(rowData.CustomerName2.value);
                rowData.Name2Salutation = {value: n2.title, valid: true};
                rowData.Name2First = {value: n2.first, valid: true};
                rowData.Name2Middle = {value: n2.middle, valid: true};
                rowData.Name2Last = {value: n2.last, valid: true};
                rowData.Name2Suffix = {value: n2.suffix, valid: true};
            }

            // validate row contains data and add  (If a row contains labels instead of data, don't add it)
            if (rowData.ResortName.value != 'ResortName' && rowData.ResortName.value != ''&& rowData.Contract.value != '' ) {
                inputData.records.push(rowData);
            }
        });

    }
	/*
		Call Salesforce to get data used in validation of incoming data
	*/
    that.getValidationData = function() {
		var recordParam = [];
        var contractNumberList = [];
        var resortNameList = [];
        var unitNumberList = {};
		var weekNumberList = {};
        var xlRows = inputData.records;
		DEBUG && console.log(inputData.records);
		// Build object to pass as parameter to Salesforce
        xlRows.forEach(function (xlRow) {
			var rec = {};
			rec.Contract = xlRow.Contract.value;
			rec.ContractId = xlRow.ContractId.value;
			rec.ResortName = xlRow.ResortName.value;
			rec.tu_unit_number1 = xlRow.tu_unit_number1.value;
			rec.wk_number1 = xlRow.wk_number1.value;
			rec.inventory_usage_type1 = xlRow.inventory_usage_type1.value;
			rec.Ownership = xlRow.Ownership.value;
			rec.RowNumber = xlRow.RowNumber.value;

			recordParam.push(rec);
            contractNumberList.push(xlRow.Contract.value);
            resortNameList.push(xlRow.ResortName.value);

			if (xlRow.ResortName.value != '' && xlRow.tu_unit_number1.value != '' ) {
				if (unitNumberList[xlRow.ResortName.value] == null) {
					unitNumberList[xlRow.ResortName.value] = [];
				}
				unitNumberList[xlRow.ResortName.value].push(xlRow.tu_unit_number1.value);
			}
			if (xlRow.ResortName.value != '' && xlRow.wk_number1.value != '' ) {
				if (weekNumberList[xlRow.ResortName.value] == null) {
					weekNumberList[xlRow.ResortName.value] = [];
				}
				var itemName = '00' + xlRow.wk_number1.value;
				itemName =  itemName.substr(itemName.length -2);
				weekNumberList[xlRow.ResortName.value].push(itemName);
			}

        });
		DEBUG && console.log('Data passed to SFDC as param to BatchContractsController.getGrandPacificValidationData:', recordParam);
        // JS Remoting Call to get records out of SF
        /// ASYNCHRONOUS call ///
		BatchContractsController.getGrandPacificValidationData(
			recordParam,	
            function(result) { // begin asynchronous call
                DEBUG && console.log('Async getGrandPacificValidationData result', result);
				// Update each input record with values from SF
				// For Each record that came in from the spreadsheet
				xlRows.forEach(function (xlRow) { 
					// Find the corresponding record that was returned with additional data from Salesforce
					var sfData = jQuery.grep(result, function(e){ return e.RowNumber == xlRow.RowNumber.value; });
					// update the incoming data record with the Salesforce data  
					// grep returns an array, but should only have one item.
					sfData.forEach( function(sfRecord) {
						if (sfRecord.DeveloperId != null) {
							xlRow.DeveloperId.value = sfRecord.DeveloperId;
						}
						if (sfRecord.ResortId != null) {
							xlRow.ResortId.value = sfRecord.ResortId;
						}
						if (sfRecord.DeededContractId != null) {
							xlRow.DeededContractId.value = sfRecord.DeededContractId;
						}
						if (sfRecord.ContractCode != null) {
							xlRow.ContractCode.value = sfRecord.ContractCode;
						}
						if (sfRecord.UnitInventoryId != null) {
							xlRow.UnitInventoryId.value = sfRecord.UnitInventoryId;
						} else {
							xlRow.UnitInventoryId.valid = false;
						}
						if (sfRecord.UnitInventoryICN != null) {
							xlRow.UnitInventoryICN.value = sfRecord.UnitInventoryICN;
						}
						if (sfRecord.UnitInventoryICNOccupied != null) {
							xlRow.UnitInventoryICNOccupied.value = sfRecord.UnitInventoryICNOccupied;
						}
						if (sfRecord.ResortOwnershipId != null) {
							xlRow.ResortOwnershipId.value = sfRecord.ResortOwnershipId;
							xlRow.ResortOwnershipCode.value = sfRecord.ResortOwnershipCode;
						}


					});
                });
                that.validateRecords();
                that.displayRecords();
                $j("#closingDate, #submit-load, #dataTable").show();
            } // end asynchronous call
        );
		//inputData.meta.developerId = 'a11W00000015yub';
		$j("#submit-load").show();
        ////////////////////////
    }

	/*
		Function to perform the full validation of incoming raw data before loading to SF
	*/
    that.validateRecords = function() {
        var records = inputData.records;
		inputData.meta.unverified = 0;
        records.forEach(function (record) {

			if (record["ResortId"].value == '') {
				record["ResortId"].valid = false;
				record["ResortName"].valid= false;
                var problemMessage = 'Resort not found.';
                record["Problems"] = setProblemMessage(record, problemMessage);
			}
			if (record["DeveloperId"].value == '') {
				record["DeveloperId"].valid = false;
                var problemMessage = 'Developer not found.';
				if (record["ResortId"].valid == true) {
					record["Problems"] = setProblemMessage(record, problemMessage);
				}
			}

            // Interval is Annual, Odd, or Even
            if (
				record["inventory_usage_type1"].value != 'Annual' 
				&& record["inventory_usage_type1"].value != 'Odd' 
				&& record["inventory_usage_type1"].value != 'Even' ) 
			{
                record["inventory_usage_type1"].valid = false;
                var problemMessage = 'A valid usage type has not been provided.  Must be Annual, Odd, or Even.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Interval is Annual, Odd, or Even
            if (record["tu_unit_number1"].value == '' ) {
                record["tu_unit_number1"].valid = false;
                var problemMessage = 'No Unit Number provided.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Week Number is not blank
            if (record["wk_number1"].value == '' ) {
                record["wk_number1"].valid = false;
                var problemMessage = 'Week Number not provided.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            // Week Number is between 1 and 52
            if (!(record["wk_number1"].value > 0 && record["wk_number1"].value <53) ) {
                record["wk_number1"].valid = false;
                var problemMessage = 'Week Number must be between 1 and 52.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Contract Number is not blank
            if (record["ContractId"].value == '' ) {
                record["ContractId"].valid = false;
                var problemMessage = 'Contract Number not provided.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
			// Deeded Contract already exists
            if (record["DeededContractId"].value != '' ) {
                record["DeededContractId"].valid = false;
				record["ContractId"].valid = false;
                var problemMessage = 'Deeded Contract Number already exists.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

			// Unit exists
            if (record["UnitInventoryId"].value == '' ) {
                record["UnitInventoryId"].valid = false;
				record["UnitInventoryICN"].valid = false;
				record["tu_unit_number1"].valid = false;
                var problemMessage = 'Unit Inventory does not exist.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

			// Unit available
            if (record["UnitInventoryICNOccupied"].value == true ) {
                record["UnitInventoryICNOccupied"].valid = false;
                var problemMessage = 'Inventory is occupied.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
			// Ownership Code is valid
            if (record["Ownership"].value == '' ) {
                record["Ownership"].valid = false;
                var problemMessage = 'Ownership code not provided';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
			 if (record["Ownership"].value != '' && record["ResortOwnershipId"].value == '' ) {
                record["Ownership"].valid = false;
                var problemMessage = 'Ownership code does not exist';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }


			// Currency fields valid
            if (!isNumeric(record["ContractPrice"].value)){
                record["ContractPrice"].valid = false;
                var problemMessage = 'ContractPrice value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["Financed"].value)){
                record["Financed"].valid = false;
                var problemMessage = 'Financed value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["PreviousLoanTransfer"].value)){
                record["PreviousLoanTransfer"].valid = false;
                var problemMessage = 'PreviousLoanTransfer value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["FundedAmount"].value)){
                record["FundedAmount"].valid = false;
                var problemMessage = 'FundedAmount value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["TransferEquity"].value)){
                record["TransferEquity"].valid = false;
                var problemMessage = 'TransferEquity value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["DownPayment"].value)){
                record["DownPayment"].valid = false;
                var problemMessage = 'DownPayment value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["PercentOfDP"].value)){
                record["PercentOfDP"].valid = false;
                var problemMessage = 'PercentOfDP value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["ClosingCost"].value)){
                record["ClosingCost"].valid = false;
                var problemMessage = 'ClosingCost value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["Interval"].value)){
                record["Interval"].valid = false;
                var problemMessage = 'Interval value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["Term"].value)){
                record["Term"].valid = false;
                var problemMessage = 'Term value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["APR"].value)){
                record["APR"].valid = false;
                var problemMessage = 'APR value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["PayAmount"].value)){
                record["PayAmount"].valid = false;
                var problemMessage = 'PayAmount value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["NetCommissionSalesPrice"].value)){
                record["NetCommissionSalesPrice"].valid = false;
                var problemMessage = 'NetCommissionSalesPrice value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["FdiPointsProvided"].value)){
                record["FdiPointsProvided"].valid = false;
                var problemMessage = 'FdiPointsProvided value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["CommissionDiscountAmount"].value)){
                record["CommissionDiscountAmount"].valid = false;
                var problemMessage = 'CommissionDiscountAmount value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["FdiPointsAuthorized"].value)){
                record["FdiPointsAuthorized"].valid = false;
                var problemMessage = 'FdiPointsAuthorized value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            record["Verified"] = record["isError"] ? {value: 'false', valid: false} : {value: 'true', valid: true};
			inputData.meta.unverified = record["isError"] ? ++inputData.meta.unverified : inputData.meta.unverified;


        });
    }

	/*
		Funcion to load validated data to Salesforce
	*/
    that.loadData = function() {
        if(inputData == null) {return;}

        $j("#popupLoading").show();
        var batchRows = [];
		var ownerRows = [];
        var rowId = 0;
        var batchType = 'GrandPacific';
        var batchId = 'GrandPacific|' + Date.parse(new Date());
        DEBUG && console.log('Batch Number:', batchId);
        inputData.records.forEach(function(xlRow) {
            //if(xlRow.isError == false) {
            var rowCounter = ++rowId;
            var batchRow = new Deeded_Batch_Detail__c();

			// Detail name holds the Contract Code (GMP1234) which will later be used as the Contract Name
            batchRow.Name = xlRow.ContractCode.value; //batchId + '|' + ('00000' + rowCounter).substr(-5);
            batchRow.Import_Batch_Identifier__c = batchId;
            //batchRow.Import_Batch_Row__c = rowCounter;
			batchRow.Import_Batch_Row__c = xlRow.RowNumber.value;

			batchRow.Problems__c = xlRow.Problems.value;
            batchRow.Verified__c = xlRow.Verified.value == 'true';

			batchRow.BatchNumber__c = xlRow.BatchNumber.value;
			batchRow.ResortName__c = xlRow.ResortName.value;
			batchRow.Contract__c = xlRow.Contract.value;
			batchRow.ContractId__c = xlRow.ContractId.value;
			batchRow.SalesPerson__c = xlRow.SalesPerson.value;
			batchRow.TO__c = xlRow.TO.value;
			batchRow.QAM__c = xlRow.QAM.value;
			batchRow.CustomerName1__c = xlRow.CustomerName1.value;
			batchRow.CustomerName2__c = xlRow.CustomerName2.value;
			batchRow.Source__c = xlRow.Source.value;
			batchRow.Lender__c = xlRow.Lender.value;
			batchRow.ContractPrice__c = xlRow.ContractPrice.value.ToNumber();
			if (!isNaN(Date.parse(xlRow.SalesDate.value))) batchRow.SalesDate__c = xlRow.SalesDate.value.ToDate();
			batchRow.Financed__c = xlRow.Financed.value.ToNumber();
			batchRow.PreviousLoanTransfer__c = xlRow.PreviousLoanTransfer.value.ToNumber();
			batchRow.FundedAmount__c = xlRow.FundedAmount.value.ToNumber();
			batchRow.TransferEquity__c = xlRow.TransferEquity.value.ToNumber();
			batchRow.DownPayment__c = xlRow.DownPayment.value.ToNumber();
			batchRow.PercentOfDP__c = xlRow.PercentOfDP.value.ToNumber();
			batchRow.ClosingCost__c = xlRow.ClosingCost.value.ToNumber();
			batchRow.FICO__c = xlRow.FICO.value;
			batchRow.Interval__c = xlRow.Interval.value.ToNumber();
			batchRow.Term__c = xlRow.Term.value.ToNumber();
			batchRow.APR__c = xlRow.APR.value.ToNumber();
			batchRow.PayAmount__c = xlRow.PayAmount.value.ToNumber();
			if (!isNaN(Date.parse(xlRow.FirstPayDate.value))) batchRow.FirstPayDate__c = xlRow.FirstPayDate.value.ToDate();
			batchRow.NetCommissionSalesPrice__c = xlRow.NetCommissionSalesPrice.value.ToNumber();
			batchRow.SalesCenter__c = xlRow.SalesCenter.value;
			batchRow.FdiPointsProvided__c = xlRow.FdiPointsProvided.value.ToNumber();
			batchRow.tu_unit_number1__c = xlRow.tu_unit_number1.value;
			batchRow.wk_number1__c = xlRow.wk_number1.value.ToNumber();
			batchRow.Usage__c = xlRow.Usage.value;
			batchRow.inventory_usage_type1__c = xlRow.inventory_usage_type1.value;
			batchRow.Ownership__c = xlRow.Ownership.value;
			batchRow.CommissionDiscountAmount__c = xlRow.CommissionDiscountAmount.value.ToNumber();
			batchRow.FdiPointsAuthorized__c = xlRow.FdiPointsAuthorized.value.ToNumber();

			batchRow.ResortId__c = xlRow.ResortId.value;
			batchRow.DeveloperId__c = xlRow.DeveloperId.value;
			batchRow.Unit_InventoryId__c = xlRow.UnitInventoryId.value;
			batchRow.Unit_Inventory_ICN__c = xlRow.UnitInventoryICN.value;
			batchRow.Resort_OwnershipId__c = xlRow.ResortOwnershipId.value;

            batchRows.push(batchRow);

			var ownerRow = new Deeded_Batch_Detail_Owner__c();
			ownerRow.Ordinal__c = 1;
			ownerRow.Import_Batch_Row__c = xlRow.RowNumber.value;
			ownerRow.Title__c = xlRow.Name1Salutation.value;
            ownerRow.First_Name__c = xlRow.Name1First.value;
            ownerRow.Middle_Name__c = xlRow.Name1Middle.value;
            ownerRow.Last_Name__c = xlRow.Name1Last.value;
            ownerRow.Suffix__c = xlRow.Name1Suffix.value;
			ownerRows.push(ownerRow);

			var ownerRow2 = new Deeded_Batch_Detail_Owner__c();
            ownerRow2.Ordinal__c = 2;
			ownerRow2.Import_Batch_Row__c = xlRow.RowNumber.value;
			ownerRow2.Title__c = xlRow.Name2Salutation.value;
            ownerRow2.First_Name__c = xlRow.Name2First.value;
            ownerRow2.Middle_Name__c = xlRow.Name2Middle.value;
            ownerRow2.Last_Name__c = xlRow.Name2Last.value;
            ownerRow2.Suffix__c = xlRow.Name2Suffix.value;
			ownerRows.push(ownerRow2);

			if (inputData.meta.developerId == '' || inputData.meta.developerId == null ) {
				inputData.meta.developerId = xlRow.DeveloperId.value;
			}
            //}
        });

		DEBUG && console.log('pre load data:', batchRows);
		DEBUG && console.log('pre load data (owners):', ownerRows);
		DEBUG && console.log('inputData.meta.developerId', inputData.meta.developerId);
		/// ASYNCHRONOUS call ///
        BatchContractsController.loadData(
            batchRows, ownerRows, batchType, batchId, inputData.meta.developerId,
            function(result) { // begin asynchronous call
                DEBUG && console.log('Async loadData result: ', result);
                var bi = $j('<span />');
//                for(var prop in result.errors) {
//                    DEBUG && console.log(prop + '=' + result.errors[prop]);
//                }
                bi.append('<strong>Records created in Salesforce:</strong>');
                bi.append('<br />');
				bi.append('Batch detail records: ' + result.records['deeded_batch_detail__c']);
                bi.append('<br />');
                bi.append('Contract records: ' + result.records['Deeded_Contract__c']);
                bi.append('<br />');
                bi.append('Document records: ' + result.records['Deeded_Document__c']);
                bi.append('<br />');
                bi.append('Owner records: ' + result.records['Deeded_Owner__c']);
                bi.append('<br />');
                bi.append('<br />');
                bi.append('<br />');
                $j('#insertResult').html(bi);

                var link = $j('<span />');
                link.append('<strong>Link to this batch record:</strong>');
                link.append('<br />');
                var anchor = $j('<a href="/' + result.records['Deeded_Batch_Header__c'] + '">Batch Record</a>');
                link.append(anchor);
                $j('#batchRecordLink').html(link);
                $j('#submit-load').hide();

                $j("#popupLoading").hide();

            }
        );
    }



    return that;
}
