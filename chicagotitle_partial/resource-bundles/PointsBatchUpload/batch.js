$j = jQuery.noConflict();
var inputData;
var bp = {}; 



$j(document).ready(function($) {
    $j("#popupLoading").show();
    $j("#popupEdit").hide();
    $j("#newOwner").hide(); 

    bp = new BatchParse; 
    $j('#closingDatePicker').val(new Date().toDateInputValue());    
/*    $j('#inputText').on("paste", function(){
        setTimeout(function() {
            $j("#popupLoading").show();
            getInputData();
            getParser();
            setButtonEvents();
            bp.quickParse(); 

        }, 500);
    });*/
    $j('#inputText').on("input selectionchange propertychange", function(){
            $j("#popupLoading").show();
            getInputData();
            if (inputData.meta.developer != '') {
                getParser();
                setButtonEvents();
                bp.quickParse();
            }
            $j("#popupLoading").hide();
    });

    $j("#closingDate, #expirationDate, #submit-load, #dataTable, #submit-parse, #add-contact, #edit-content, #save-content").hide();
    $j("#popupLoading").hide();


});



function getInputData() {
    var txt = $j('#inputText').val();
    inputData = Papa.parse(txt, {delimiter: "",newline: ""});
    inputData.meta.unverified = 0;
    inputData.meta.developer = '';
    if (inputData.data[0].length == 37) inputData.meta.developer = 'Welk';
    if (inputData.data[0].length == 31) inputData.meta.developer = 'Shell';
    //inputData.meta.developer = inputData.data[0].length > 35 ? 'Welk' : 'Shell';
    console.log("Input datas column count: ", inputData.data[0].length);
    console.log(inputData.meta.developer);
}

function getParser() {

    if (inputData.meta.developer == 'Welk') { bp = WelkParse(); }
    if (inputData.meta.developer == 'Shell') { bp = ShellParse(); }
}



function setButtonEvents() {
    $j('#submit-parse').click(bp.parseData);
    $j('#ExpirationPickList').change(bp.parseData);
    $j('#closingDatePicker').change(bp.parseData);
    $j('#submit-load').click(bp.loadData);
    $j('#savebutton').click(bp.saveContent);
    $j('#cancelbutton').click(bp.cancelChange);

    //dynamic edit button    
    $j('#edit-row').live('click', function() {
        $j("#popupEdit").show();
        
        //get row id
        var rowid = this.id;
        var trid = $j(this).closest('tr').attr('id');
        console.log(trid);

        $j("#editRow").text(rowid);

        //find rows to edit in popup by row id
        var $rowCells = $j(this).closest("tr").find("td");
        
        var curID = $rowCells.eq(1).text();
        var curMember = $rowCells.eq(3).text();
        var curNum = $rowCells.eq(4).text();
        var curDate = $rowCells.eq(5).text();
        var curBat = $rowCells.eq(6).text();
        var curPort = $rowCells.eq(7).text();
        var curNameOne = $rowCells.eq(8).text();
        var curNameTwo = $rowCells.eq(9).text();
        var curAdd = $rowCells.eq(10).text();
        var curCity = $rowCells.eq(11).text();
        var curState = $rowCells.eq(12).text();
        var curZip = $rowCells.eq(13).text();
        var curCount = $rowCells.eq(14).text();
        var curVest = $rowCells.eq(15).text();
        var curUp = $rowCells.eq(16).text();
        var curCp = $rowCells.eq(17).text();
        var curNp = $rowCells.eq(18).text();
        var curSp = $rowCells.eq(19).text();
        //end Sum Pts
        var curOy = $rowCells.eq(20).text();
        var curIc = $rowCells.eq(21).text();
        var curRc = $rowCells.eq(22).text();
        var curMcbp = $rowCells.eq(23).text();
        var curPrice = $rowCells.eq(24).text();
        var curFee = $rowCells.eq(25).text();
        var curVal = $rowCells.eq(26).text();
        var curFin = $rowCells.eq(27).text();
        var curDown = $rowCells.eq(28).text();
        var curCeq = $rowCells.eq(29).text();
        var curDeq = $rowCells.eq(30).text();
        var curTeq = $rowCells.eq(31).text();
        var curCls = $rowCells.eq(32).text();
        var curTotcash = $rowCells.eq(33).text();
        var curCtt = $rowCells.eq(34).text();
        var curClose = $rowCells.eq(35).text();
        var curEsc = $rowCells.eq(36).text();
        var curHyp = $rowCells.eq(37).text();
        var curDeed = $rowCells.eq(38).text();
        var curFirstp = $rowCells.eq(39).text();
        var curCloseDate = $rowCells.eq(40).text();
        var curTrust = $rowCells.eq(41).text();
        var curLend = $rowCells.eq(42).text();
        var curSea = $rowCells.eq(43).text();
        var curRes = $rowCells.eq(44).text();
        var curTotDeb = $rowCells.eq(45).text();
        var curTotCrd = $rowCells.eq(46).text();

        
        $j("#txtID").val(trid);
        $j("#txtNum").val(curMember);
        $j("#conNum").val(curNum);
        $j("#conDate").val(curDate);
        $j("#batNum").val(curBat);
        $j("#portfolio").val(curPort);
        $j("#nameOne").val(curNameOne);
        $j("#nameTwo").val(curNameTwo);
        $j("#address").val(curAdd);
        $j("#city").val(curCity);
        $j("#state").val(curState);
        $j("#zip").val(curZip);
        $j("#country").val(curCount);
        $j("#vesting").val(curVest);
        $j("#upgrdPts").val(curUp);
        $j("#convrtPts").val(curCp);
        $j("#newPts").val(curNp);  
        $j("#sumPts").val(curSp);  
        //end Sum Pts
        $j("#occYear").val(curOy); 
        $j("#ic").val(curIc);  
        $j("#rc").val(curRc);  
        $j("#mcbp").val(curMcbp); 
        $j("#purPrice").val(curPrice);  
        $j("#convFee").val(curFee); 
        $j("#convVal").val(curVal); 
        $j("#amtFin").val(curFin);  
        $j("#downPmt").val(curDown); 
        $j("#closeEq").val(curCeq);
        $j("#downEq").val(curDeq); 
        $j("#totalEq").val(curTeq); 
        $j("#clsCost").val(curCls); 
        $j("#totCash").val(curTotcash); 
        $j("#cttClose").val(curCtt); 
        $j("#closeCost").val(curClose);  
        $j("#escPro").val(curEsc);  
        $j("#hypAmt").val(curHyp);  
        $j("#deedDate").val(curDeed);  
        $j("#firstPay").val(curFirstp); 
        $j("#closeDate").val(curCloseDate);  
        $j("#trust").val(curTrust);  
        $j("#lender").val(curLend);  
        $j("#season").val(curSea);
        $j("#resortCode").val(curRes); 
        $j("#totDeb").val(curTotDeb);
        $j("#totCred").val(curTotCrd);  
    });

    $j("#popupEdit input").keyup(function() {
            var value = $j(this).val();
            $j("#popupEdit td").text(value);

         })
         .keyup();

    $j("#add-contact").live("click", function() {
        $j("#newOwner").show(); 
        $j("#newInp").hide(); 

        var newDiv = $j('#newOwner');
        var i = $j('#newOwner p').size();
        $j('<p><label for="newLab"><input type="text" class="inputRe" name="newInp" id="newInp_' + i +'" value="" placeholder="Last Name, First Name" /></label></p>').appendTo(newDiv);
                i++;
                return false;
    });  

         
}


function BatchParse() {
    var that = {};
    that.sfContracts = {};
    that.sfLenders = {};
    that.sfMembershipType = {};
    that.sfResort = {};
    that.sfExpirationDate = {};
    that.sfMemberType = {};
    that.sfTransactionType = {};


    that.displayRecords = function() {
        $j("#dataTable").show();
        var table = $j('<table />');
        var headers = inputData.headers;
        var records = inputData.records;

        

        if ( headers == null ) return;
        var headerRow = $j('<tr />');
        headers.forEach(function (header) {
            if(header.visible == true) {
                headerRow.append('<th class="thG">'+ header.label+'</th>');
            }
        });
        table.append(headerRow);

        var row_id = 1;
        

        if (records != null) {
            records.forEach(function (record, index) {
                if (!record.isError) {
                    row_id++;

                    row = $j('<tr id="' + row_id + '"/>');

                } else {
                    row = $j('<tr class="rowError" />');
                }
                headers.forEach(function (header, index) {
                    if(header.visible == true) {

                        if(header.name == 'Edit') {
                            row.append('<td><button id="edit-row">Edit</button></td>');
                        } else {

                            if (header.inputColumn != 0) {
                                
                                if (record[header.name].valid == true) {
                                    row.append('<td id="' + record[header.name].value + '">' + record[header.name].value + '</td>');
                                } else {
                                    row.append('<td class="cellError">' + record[header.name].value + '</td>');
                                }
                            } else {
                                if (record[header.name].valid == true) {
                                    row.append('<td class="cellCalculated">' + record[header.name].value + '</td>');
                                } else {
                                    row.append('<td class="cellError">' + record[header.name].value + '</td>');
                                }
                            }
                        }
                    }

                });
                table.append(row);
            });
        }

        

        $j('#parsedDataTable').html(table);

        // Update UI with restults information
        var bi = $j('<span />');
        bi.append('<strong>Parse Results:</strong>');
        bi.append($j('<br/>'));
        bi.append('Source format: ' + inputData.meta.developer);
        bi.append($j('<br/>'));
        bi.append(inputData.records.length + ' Records');
        bi.append($j('<br/>'));
        if (inputData.meta.unverified != 1) {
            bi.append(inputData.meta.unverified + ' Unverified Records');
        } else {
            bi.append(inputData.meta.unverified + ' Unverified Record');
        }
        $j('#parseResult').html(bi);
        $j("#popupLoading").hide();
    }

    return that;

}




