public with sharing class UnitSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Unit__c.Id,
			Unit__c.Name,
			Unit__c.ResortId__c,
			Unit__c.Search_Code__c,
			Unit__c.ICN_Code__c
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Unit__c.sObjectType;
	}

	public List<Unit__c> selectById(Set<ID> idSet)
	{
		return (List<Unit__c>) selectSObjectsById(idSet);
	}
	
	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Unit__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Unit__c>) Database.query(query.toSOQL());
	}

	public List<Unit__c> selectByName(Set<String> nameSet)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameSet');
		return  (List<Unit__c>) Database.query(query.toSOQL());
	}



}