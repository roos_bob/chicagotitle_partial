public class Transaction_AfterDelete extends TriggerHandlerBase {
	public override void mainEntry(TriggerParameters tp) {
		System.debug('Transaction_AfterDelete: ' + tp);
        Transaction_Helper.updateContractTransactionLookups(tp.oldList);
		Transaction_Helper.setCurrentOwners(tp.oldList);
    }

}