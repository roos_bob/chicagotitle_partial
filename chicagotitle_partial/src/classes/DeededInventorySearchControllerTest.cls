@isTest
private class DeededInventorySearchControllerTest {
	@testSetup static void setupData() {
	// Create Trigger Setting
		CS_Trigger_Setting__c setting = new CS_Trigger_Setting__c();
		setting.Name = 'Deeded_Owner_List__c.setLookups';
		setting.Enabled__c = true;
		insert setting;

	// Create Developer
		Map<String, Id> developerRecordTypes =  UtilityClass.getRecordTypeIdsByDeveloperName(Developer__c.SObjectType); 
		Id developerRTId;

		if(developerRecordTypes.containsKey('Deeded')){
			developerRTId = developerRecordTypes.get('Deeded');
		} else {
			System.assert(false, 'Missing Deeded record type for Developer__c');
		}

		Developer__c d = new Developer__c(Name = 'Grand Pacific', Active__c = true, RecordTypeId = developerRTId);
		insert d;

	// Create Resort
		Map<String, Id> resortRecordTypes =  UtilityClass.getRecordTypeIdsByDeveloperName(Resort__c.SObjectType); 
		Id resortRTId;

		if(resortRecordTypes.containsKey('Deeded')){
			resortRTId = resortRecordTypes.get('Deeded');
		} else {
			System.assert(false, 'Missing Deeded record type for Resort__c');
		}

		Resort__c r = new Resort__c(Name = 'Grand Pacific MarBrisa Resort', RecordTypeId = resortRTId, Developer__c = d.Id, Code__c = 'GMP');
		insert r;

	// Create Weeks
		Map<String, Week__c> wks = new Map<String, Week__c>();
		wks.put('01', new Week__c(Name = '01', ICN_Code__c = '01', Resort__c = r.Id, Season__c = 'Gold'));
		insert wks.values();

	// Create Usage Types
		Map<String, Usage_Type__c> uts = new Map<String, Usage_Type__c>();
		uts.put('Annual', new Usage_Type__c(Name = 'Annual', ICN_Code__c = 'Z', Resort__c = r.Id));
		uts.put('Biennial (Odd)', new Usage_Type__c(Name = 'Biennial (Odd)', ICN_Code__c = 'O', Resort__c = r.Id));
		uts.put('Biennial (Even)', new Usage_Type__c(Name = 'Biennial (Even)', ICN_Code__c = 'E', Resort__c = r.Id));
		insert uts.values();

	// Create Overlapping Usage Types
		List<Overlapping_Usage_Type__c> outs = new List<Overlapping_Usage_Type__c>();
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Annual').Id, Overlaps_WithId__c = uts.get('Biennial (Odd)').Id));
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Annual').Id, Overlaps_WithId__c = uts.get('Biennial (Even)').Id));
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Biennial (Odd)').Id, Overlaps_WithId__c = uts.get('Annual').Id));
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Biennial (Even)').Id, Overlaps_WithId__c = uts.get('Annual').Id));
		insert outs;

	// Create Section Types
		Map<String, Section_Type__c> sts = new Map<String, Section_Type__c>();
		sts.put('Full', new Section_Type__c(Name = 'Full', ResortId__c = r.Id));
		sts.put('Bedroom', new Section_Type__c(Name = 'Bedroom', ResortId__c = r.Id));
		sts.put('Studio', new Section_Type__c(Name = 'Studio', ResortId__c = r.Id));
		insert sts.values();

	// Create Overlapping Section Types
		List<Overlapping_Section_Type__c> osts = new List<Overlapping_Section_Type__c>();
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Full').Id, Overlaps_WithId__c = sts.get('Bedroom').Id));
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Full').Id, Overlaps_WithId__c = sts.get('Studio').Id));
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Bedroom').Id, Overlaps_WithId__c = sts.get('Full').Id));
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Studio').Id, Overlaps_WithId__c = sts.get('Full').Id));
		insert osts;

	// Create Unit
		Unit__c u = new Unit__c(Name = '5811', ResortId__c = r.Id, ICN_Code__c = '5811', Type__c = '2.7', View__c = 'Park');
		insert u;

	// Create Unit Sections
		Map<String, Unit_Section__c> uss = new Map<String, Unit_Section__c>();
		uss.put('5811', new Unit_Section__c(Name = '5811', ICN_Code__c = 'A1', UnitId__c = u.Id, Size__c = '2BR/2BA', Section_TypeId__c = sts.get('Full').Id));
		uss.put('58111', new Unit_Section__c(Name = '58111', ICN_Code__c = 'B1', UnitId__c = u.Id, Size__c = '1BR/1BA', Section_TypeId__c = sts.get('Bedroom').Id));
		uss.put('58112', new Unit_Section__c(Name = '58112', ICN_Code__c = 'D1', UnitId__c = u.Id, Size__c = 'STUDIO/1BA', Section_TypeId__c = sts.get('Studio').Id));
		insert uss.values();

	// Create Unit Inventories
		Map<String, Unit_Inventory__c> uis = new Map<String, Unit_Inventory__c>();
		// Full, Even, Wk 1
		uis.put('Full/Even/Wk1', new Unit_Inventory__c(Name = 'GMP581101A1E', ResortId__c = r.Id, Unit_SectionId__c = uss.get('5811').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Even)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('5811').Id + ' ' + uts.get('Biennial (Even)').Id + ' ' + wks.get('01').Id));
		// Full, Odd, Wk 1
		uis.put('Full/Odd/Wk1', new Unit_Inventory__c(Name = 'GMP581101A1O', ResortId__c = r.Id, Unit_SectionId__c = uss.get('5811').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Odd)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('5811').Id + ' ' + uts.get('Biennial (Odd)').Id + ' ' + wks.get('01').Id));
		// Full, Annual, Wk 1
		uis.put('Full/Annual/Wk1', new Unit_Inventory__c(Name = 'GMP581101A1Z', ResortId__c = r.Id, Unit_SectionId__c = uss.get('5811').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Annual').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('5811').Id + ' ' + uts.get('Annual').Id + ' ' + wks.get('01').Id));

		// Bedroom, Even, Wk 1
		uis.put('Bedroom/Even/Wk1', new Unit_Inventory__c(Name = 'GMP581101B1E', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58111').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Even)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58111').Id + ' ' + uts.get('Biennial (Even)').Id + ' ' + wks.get('01').Id));
		// Bedroom, Odd, Wk 1
		uis.put('Bedroom/Odd/Wk1', new Unit_Inventory__c(Name = 'GMP581101B1O', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58111').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Odd)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58111').Id + ' ' + uts.get('Biennial (Odd)').Id + ' ' + wks.get('01').Id));
		// Bedroom, Annual, Wk 1
		uis.put('Bedroom/Annual/Wk1', new Unit_Inventory__c(Name = 'GMP581101B1Z', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58111').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Annual').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58111').Id + ' ' + uts.get('Annual').Id + ' ' + wks.get('01').Id));

		// Studio, Even, Wk 1
		uis.put('Studio/Even/Wk1', new Unit_Inventory__c(Name = 'GMP581101D1E', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58112').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Even)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58112').Id + ' ' + uts.get('Biennial (Even)').Id + ' ' + wks.get('01').Id));
		// Studio, Odd, Wk 1
		uis.put('Studio/Odd/Wk1', new Unit_Inventory__c(Name = 'GMP581101D1O', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58112').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Odd)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58112').Id + ' ' + uts.get('Biennial (Odd)').Id + ' ' + wks.get('01').Id));
		// Studio, Annual, Wk 1
		uis.put('Studio/Annual/Wk1', new Unit_Inventory__c(Name = 'GMP581101D1Z', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58112').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Annual').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58112').Id + ' ' + uts.get('Annual').Id + ' ' + wks.get('01').Id));

		insert uis.values();

		// Set overlapping ids.  
		uis.get('Full/Even/Wk1').Overlaps_With__c = uis.get('Full/Annual/Wk1').Id + ',' + uis.get('Bedroom/Annual/Wk1').Id + ',' +
			uis.get('Bedroom/Even/Wk1').Id + ',' + uis.get('Studio/Annual/Wk1').Id + ',' + uis.get('Studio/Even/Wk1').Id;

		uis.get('Full/Odd/Wk1').Overlaps_With__c = uis.get('Full/Annual/Wk1').Id + ',' + uis.get('Bedroom/Annual/Wk1').Id + ',' +
			uis.get('Bedroom/Odd/Wk1').Id + ',' + uis.get('Studio/Annual/Wk1').Id + ',' + uis.get('Studio/Odd/Wk1').Id;

		uis.get('Full/Annual/Wk1').Overlaps_With__c = uis.get('Full/Odd/Wk1').Id + ',' + uis.get('Full/Even/Wk1').Id + ',' + 
			uis.get('Bedroom/Annual/Wk1').Id + ',' + uis.get('Bedroom/Even/Wk1').Id + ',' + uis.get('Bedroom/Odd/Wk1').Id + ',' + 
			uis.get('Studio/Annual/Wk1').Id + ',' + uis.get('Studio/Even/Wk1').Id + ',' + uis.get('Studio/Odd/Wk1').Id;

		uis.get('Bedroom/Annual/Wk1').Overlaps_With__c = uis.get('Full/Odd/Wk1').Id + ',' + uis.get('Full/Even/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' +
			uis.get('Bedroom/Even/Wk1').Id + ',' + uis.get('Bedroom/Odd/Wk1').Id;

		uis.get('Bedroom/Even/Wk1').Overlaps_With__c = uis.get('Full/Even/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' +
			uis.get('Bedroom/Annual/Wk1').Id;

		uis.get('Bedroom/Odd/Wk1').Overlaps_With__c = uis.get('Full/Odd/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' +
			uis.get('Bedroom/Annual/Wk1').Id;

		uis.get('Studio/Annual/Wk1').Overlaps_With__c = uis.get('Full/Odd/Wk1').Id + ',' + uis.get('Full/Even/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' + 
			uis.get('Studio/Even/Wk1').Id + ',' + uis.get('Studio/Odd/Wk1').Id;

		uis.get('Studio/Even/Wk1').Overlaps_With__c = uis.get('Full/Even/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' +
			uis.get('Studio/Annual/Wk1').Id;

		uis.get('Studio/Odd/Wk1').Overlaps_With__c = uis.get('Full/Odd/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' +
			uis.get('Studio/Annual/Wk1').Id;

		update uis.values();

		// Create contract
		Deeded_Contract__c c1 = new Deeded_Contract__c(Name = 'contract1', Contract_Code__c = 'contract1', Unit_InventoryId__c = uis.get('Full/Even/Wk1').Id);
		insert c1;

		Deeded_Contract__c c2 = new Deeded_Contract__c(Name = 'contract2', Contract_Code__c = 'contract1', Unit_InventoryId__c = uis.get('Full/Odd/Wk1').Id);
		insert c2;

		// Create documents
		Deeded_Document__c doc1 = new Deeded_Document__c(Name = 'doc1', Deeded_Contract__c = c1.Id);
		insert doc1;

		Deeded_Document__c doc2 = new Deeded_Document__c(Name = 'doc2', Deeded_Contract__c = c2.Id);
		insert doc2;

		// Create owners
		Deeded_Owner__c o1 = new Deeded_Owner__c(Name = 'owner1');
		insert o1;

		Deeded_Owner__c o2 = new Deeded_Owner__c(Name = 'owner2');
		insert o2;

		// Create owner lists
		Deeded_Owner_List__c ol1 = new Deeded_Owner_List__c(Deeded_Document__c = doc1.Id, Deeded_Owner__c = o1.Id);
		insert ol1;

		Deeded_Owner_List__c ol2 = new Deeded_Owner_List__c(Deeded_Document__c = doc2.Id, Deeded_Owner__c = o2.Id);
		insert ol2;

	}

	@isTest static void testProperties() {
		DeededInventorySearchController ctrl = new DeededInventorySearchController();

		List<Unit_Inventory__c> uis = ctrl.unitInventories;
		List<Deeded_Contract__c> cs = ctrl.contracts;
		List<Deeded_Document__c> ds = ctrl.documents;
		List<Deeded_Owner__c> os = ctrl.owners;

		String s = ctrl.unitInventorySize;
		s = ctrl.contractSize;
		s = ctrl.documentSize;
		s = ctrl.ownerSize;

		List<Developer__c> devs = ctrl.developers;
		List<Deeded_Owner_Type__c> ots = ctrl.ownerTypes;

		List<String> listStr = ctrl.views;
		listStr = ctrl.seasons;
		listStr = ctrl.sizes;

		Set<String> setStr = ctrl.usageTypes;
		setStr = ctrl.phases;
		setStr = ctrl.documentTypes;

		List<DeededInventorySearchController.PicklistItem> resorts = DeededInventorySearchController.getMatchingResorts(String.valueOf(devs[0].Id), 'mar');
		List<DeededInventorySearchController.PicklistItem> batchNumbers = DeededInventorySearchController.getMatchingBatchNumbers('1');

	}

	@isTest static void testSearch1() {
		DeededInventorySearchController ctrl = new DeededInventorySearchController();

		PageReference pg = Page.DeededInventorySearch;
		Test.setCurrentPage(pg);

		Datetime d = Datetime.now();
		String dtStr1 = d.format('MM/dd/YYYY');
		String dtStr2 = d.addDays(1).format('MM/dd/YYYY');

		Id rId = [SELECT Id FROM Resort__c LIMIT 1].Id;

		ApexPages.currentPage().getParameters().put('resortId', rId);
		ApexPages.currentPage().getParameters().put('sObjectType', 'Unit_Inventory__c');

		ApexPages.currentPage().getParameters().put('unitMatchAll', 'on');
		ApexPages.currentPage().getParameters().put('unit', '5811');
		ApexPages.currentPage().getParameters().put('view', 'Park');
		ApexPages.currentPage().getParameters().put('week', '01');
		ApexPages.currentPage().getParameters().put('season', 'Gold');
		ApexPages.currentPage().getParameters().put('usageType', 'Biennial (Even)');
		ApexPages.currentPage().getParameters().put('phase', '1');
		ApexPages.currentPage().getParameters().put('size', '2BR/2BA');
		ApexPages.currentPage().getParameters().put('available', 'Yes');

		ApexPages.currentPage().getParameters().put('contractMatchAll', 'on');
		ApexPages.currentPage().getParameters().put('contractCode', 'contract1');
		ApexPages.currentPage().getParameters().put('contractCodeExactMatch', 'on');
		ApexPages.currentPage().getParameters().put('contractStatus', 'Active');
		ApexPages.currentPage().getParameters().put('createDateOn', 'false');
		ApexPages.currentPage().getParameters().put('createDateStart', dtStr1);
		ApexPages.currentPage().getParameters().put('createDateEnd', dtStr2);
		ApexPages.currentPage().getParameters().put('updateDateOn', 'false');
		ApexPages.currentPage().getParameters().put('updateDateStart', dtStr1);
		ApexPages.currentPage().getParameters().put('updateDateEnd', dtStr2);

		Test.startTest();

		ctrl.initResortId();
		ctrl.runSearch();

		Test.stopTest();
	}

	@isTest static void testSearch2() {
		DeededInventorySearchController ctrl = new DeededInventorySearchController();

		PageReference pg = Page.DeededInventorySearch;
		Test.setCurrentPage(pg);

		Datetime d = Datetime.now();
		String dtStr1 = d.format('MM/dd/YYYY');
		String dtStr2 = d.addDays(1).format('MM/dd/YYYY');

		Id rId = [SELECT Id FROM Resort__c LIMIT 1].Id;

		ApexPages.currentPage().getParameters().put('resortId', rId);
		ApexPages.currentPage().getParameters().put('sObjectType', 'Unit_Inventory__c');

		ApexPages.currentPage().getParameters().put('docMatchAll', 'false');
		ApexPages.currentPage().getParameters().put('documentType', 'Grant Deed');
		ApexPages.currentPage().getParameters().put('instrumentNumber', '1');
		ApexPages.currentPage().getParameters().put('batchNumber', '1');
		ApexPages.currentPage().getParameters().put('accomodation', '1');
		ApexPages.currentPage().getParameters().put('book', '1');
		ApexPages.currentPage().getParameters().put('page', '1');
		ApexPages.currentPage().getParameters().put('recordedDateOn', 'on');
		ApexPages.currentPage().getParameters().put('recordedDateStart', dtStr1);
		ApexPages.currentPage().getParameters().put('recordedDateEnd', dtStr2);

		ApexPages.currentPage().getParameters().put('ownerMatchAll', 'false');
		ApexPages.currentPage().getParameters().put('companyName', 't');
		ApexPages.currentPage().getParameters().put('lastName', 't');
		ApexPages.currentPage().getParameters().put('firstName', 't');
		ApexPages.currentPage().getParameters().put('middleName', 't');
		ApexPages.currentPage().getParameters().put('title', 't');
		ApexPages.currentPage().getParameters().put('ownerType', 't');

		Test.startTest();

		ctrl.initResortId();

		ctrl.runSearch();

		Test.stopTest();
	}

	@isTest static void testNavigation() {
		DeededInventorySearchController ctrl = new DeededInventorySearchController();

		PageReference pg = Page.DeededInventorySearch;
		Test.setCurrentPage(pg);

		ApexPages.currentPage().getParameters().put('sortField', 'Code__c');
		ApexPages.currentPage().getParameters().put('sObjectType', 'Unit_Inventory__c');

		Test.startTest();

		ctrl.toggleSort();

		ctrl.goToNext();
		ctrl.goToEnd();
		ctrl.goToPrevious();
		ctrl.goToBeginning();

		Test.stopTest();

	}

}