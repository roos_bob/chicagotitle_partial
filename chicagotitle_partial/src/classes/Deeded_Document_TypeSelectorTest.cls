@isTest
private class Deeded_Document_TypeSelectorTest {

	@isTest
	private static void testSelector() {
		Deeded_Document_Type__c record = new Deeded_Document_Type__c(Name = 'test');
		insert record;

		Set<Id> idSet = new Set<Id> {record.Id};
		List<String> nameList = new List<String> {'test'};
		Set<String> nameSet = new Set<String> {'test'};

		Deeded_Document_TypeSelector selector = new Deeded_Document_TypeSelector();
		
		List<Deeded_Document_Type__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Deeded_Document_Type__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 1);

		List<Deeded_Document_Type__c> byNameSet = selector.selectByName(nameSet);
		System.assert(byNameSet.size() == 1);
		
		Map<String, Deeded_Document_Type__c> mapByName = selector.selectMapByName(nameSet);
		System.assert(mapByName.size() == 1);

	}
}