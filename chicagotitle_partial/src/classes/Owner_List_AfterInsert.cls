public class Owner_List_AfterInsert extends TriggerHandlerBase   {

    public override void mainEntry(TriggerParameters tp) {
		System.debug('Owner_List_AfterInsert: ' + tp);
		Owner_List_Helper.updateContractRecord(tp.newList);        
		Owner_List_Helper.setCurrentOwners(tp.newList);
    }


}