public without sharing class Transaction_Helper {
	/*
		After Insert, Update, Delete update the Contract with correct Transactions
	*/
    public static void updateContractTransactionLookups(List<Transaction__c> newList) {
		//List distinct Contracts
		Set<Id> contractIdSet = new Set<Id>();
		for (Transaction__c trans : newList) {
			contractIdSet.add(trans.ContractId__c);
		}
		List<Id> contractIdList = new List<Id>(contractIdSet);

		Map<Id, Id> latestTransactionMap = new Map<Id, Id>();
		Map<Id, Id> latestOwnerTransactionMap = new Map<Id, Id>();
		Map<Id, Id> latestPointsTransactionMap = new Map<Id, Id>();
		Map<Id, Id> latestIssuedTransactionMap = new Map<Id, Id>();
		Map<Id, Boolean> contractActiveFlagMap = new Map<Id, Boolean>();

		///////////////////////////////////
		//Latest Transaction
		List<Contract__c> latestTransactions = [Select Id, Name, (Select Id from transactions__r ORDER BY closing_Date__c desc, CreatedDate desc	limit 1)
			from contract__c where id in :contractIdList];
		for (Contract__c contract : latestTransactions ) {
			if (contract.Transactions__r.size() > 0 ) {
				latestTransactionMap.put( contract.id, contract.Transactions__r[0].Id );
			}
		}

		///////////////////////////////////
		//Last Owner Transaction
		List<Contract__c> latestOwnerTransactions = [Select Id, Name, (Select Id from transactions__r where Transaction_Type__r.Ownership__c = true ORDER BY closing_Date__c desc, CreatedDate desc	limit 1)
			from contract__c where id in :contractIdList];
		for (Contract__c contract : latestOwnerTransactions ) {
			if (contract.Transactions__r.size() > 0 ) {
				latestOwnerTransactionMap.put( contract.id, contract.Transactions__r[0].Id );
			}
		}

		///////////////////////////////////
		//Last Points Transaction
		List<Contract__c> latestPointsTransactions = [
			Select Id, Name, 
				(Select Id, Transaction_Type__r.Active__c from transactions__r where Transaction_Type__r.Points__c = true ORDER BY closing_Date__c desc, CreatedDate desc	limit 1)
			from contract__c where id in :contractIdList];
		for (Contract__c contract : latestPointsTransactions ) {
			if (contract.Transactions__r.size() > 0 ) {
				latestPointsTransactionMap.put( contract.id, contract.Transactions__r[0].Id );
				contractActiveFlagMap.put(contract.Id, contract.Transactions__r[0].Transaction_Type__r.Active__c );

			}
		}

		///////////////////////////////////
		//Last Issued Transaction
		List<Contract__c> latestIssuedTransactions = [Select Id, Name, (Select Id from transactions__r WHERE Transaction_Type__r.name = 'Issued' ORDER BY closing_Date__c desc, CreatedDate desc	limit 1)
			from contract__c where id in :contractIdList];
		for (Contract__c contract : latestIssuedTransactions ) {
			if (contract.Transactions__r.size() > 0 ) {
				latestIssuedTransactionMap.put( contract.id, contract.Transactions__r[0].Id );
			}
		}


		//////////////////////////////////////////////////////////////////////////
		List<Contract__c> contractsToUpdate = [
			select id, name, Last_Transaction__c, Last_Points_Trans__c, Last_Owner_Trans__c, Last_Issued_Trans__c from Contract__c where id in :contractIdList
		];
		for (Contract__c contract : contractsToUpdate) {
			
			contract.Last_Transaction__c = latestTransactionMap.get(contract.Id);
			contract.Last_Owner_Trans__c = latestOwnerTransactionMap.get(contract.Id);
			contract.Last_Points_Trans__c = latestPointsTransactionMap.get(contract.Id);
			contract.Last_Issued_Trans__c = latestIssuedTransactionMap.get(contract.Id);
			contract.Active__c = contractactiveFlagMap.containsKey(contract.Id) ? contractactiveFlagMap.get(contract.Id) : false;

		}
		
		Database.update(contractsToUpdate);
	}

	/*
		After Delete, update the current owners of the Contract
	*/
	public static void setCurrentOwners(List<Transaction__c> oldList) {
		Set<Id> contractIdList = new Set<Id>();

		for (Transaction__c trans : oldList) {
			contractIdList.add(trans.ContractId__c);
		}
		Contract_Helper.setCurrentOwners(contractIdList);
	}


}