@isTest
public class DeededTitlePolicyArchiveControllerTest {
	@testSetup static void setupTestData() {
	// Create Developer
		Map<String, Id> developerRecordTypes =  UtilityClass.getRecordTypeIdsByDeveloperName(Developer__c.SObjectType); 
		Id developerRTId;

		if(developerRecordTypes.containsKey('Deeded')){
			developerRTId = developerRecordTypes.get('Deeded');
		} else {
			System.assert(false, 'Missing Deeded record type for Developer__c');
		}

		Developer__c d = new Developer__c(Name = 'Grand Pacific', Active__c = true, RecordTypeId = developerRTId);
		insert d;

	// Create Resort
		Map<String, Id> resortRecordTypes =  UtilityClass.getRecordTypeIdsByDeveloperName(Resort__c.SObjectType); 
		Id resortRTId;

		if(resortRecordTypes.containsKey('Deeded')){
			resortRTId = resortRecordTypes.get('Deeded');
		} else {
			System.assert(false, 'Missing Deeded record type for Resort__c');
		}

		Resort__c r = new Resort__c(Name = 'Grand Pacific MarBrisa Resort', RecordTypeId = resortRTId, Developer__c = d.Id, Code__c = 'GMP');
		insert r;

		Deeded_Contract__c dc = new Deeded_Contract__c(Name = 'Contract1', Active__c = true, ResortId__c = r.Id);
		insert dc;
	}

	public static testMethod void test_ArchiveTitlePolicyController() {  

		Deeded_Contract__c contract = [select Id from Deeded_Contract__c limit 1];
		
		ApexPages.StandardController sc = new ApexPages.StandardController(contract);
		Test.startTest();

		DeededTitlePolicyArchiveController cn = new DeededTitlePolicyArchiveController(sc);
		cn.archiveTitlePolicy();

		Test.stopTest();
	}
}