public class Deeded_Contract_Helper {

	public static List<ImportedContractRecord> getGrandPacificValidationData(List<ImportedContractRecord> xlRows) {
		// For each record get:
		//   Developer Id
		//   Resort Id
		//   Unit Inventory Id 
		//   Resort Ownership Id
		//   Deeded Contract Id (should NOT exist)
		//  Finding the Unit Inventory Id requires knowing the ICN code [Resort][Unit][Week][Section][Usage]


		Map<string, Resort__c> resortNameMap = new Map<string, Resort__c> ();
		Map<String, Deeded_Contract__c> contractMap = new Map<String, Deeded_Contract__c> ();
		Map<String, Week__c> weekMap = new Map<String, Week__c> ();
		Map<String, Unit_Section__c> sectionMap = new Map<String, Unit_Section__c> ();
		Map<String, Usage_Type__c> usageMap = new Map<String, Usage_Type__c> ();
		Map<String, Unit_Inventory__c> inventoryMap = new Map<String, Unit_Inventory__c> ();
		Map<String, Resort_Ownership__c> ownershipMap = new Map<String, Resort_Ownership__c> ();
		Set<string> contractCodes = new Set<string> ();
		Set<string> resortNames = new Set<string> ();
		Set<string> weekCodes = new Set<string> ();
		Set<string> sectionCodes = new Set<string> ();
		Set<string> usageCodes = new Set<string> ();
		Set<string> inventoryCodes = new Set<string> ();
		Set<String> ownershipCodes = new Set<String> ();

		// First get the related resorts so we can get the Resort Code for lookups
		for (ImportedContractRecord xlData : xlRows) {
			resortNames.add(xlData.ResortName);
		}
		resortNameMap = new ResortSelector().selectMapByName(resortNames);
		for (ImportedContractRecord xlData : xlRows) {
			if (resortNameMap.get(xlData.ResortName) != null) {
				string resortCode = resortNameMap.get(xlData.ResortName).Code__c;
				xlData.DeveloperId = resortNameMap.get(xlData.ResortName).Developer__c;
				xlData.ResortId = resortNameMap.get(xlData.ResortName).Id;
				xlData.ResortICN = resortCode;
				xlData.ResortCode = resortCode;
				xlData.WeekCode = resortCode + (xlData.wk_number1.length() == 1 ? '0' : '') + xlData.wk_number1;
				xlData.SectionCode = resortCode + xlData.tu_unit_number1;
				xlData.UsageCode = resortCode + xlData.inventory_usage_type1;
				//xlData.ContractCode = resortCode + xlData.ContractId;
				xlData.ContractCode = xlData.ContractId;
				xlData.ResortOwnershipCode = resortCode + xlData.Ownership;

				weekCodes.add(xlData.WeekCode);
				sectionCodes.add(xlData.SectionCode);
				usageCodes.add(xlData.UsageCode);
				contractCodes.add(xlData.ContractCode);
				ownershipCodes.add(xlData.ResortOwnershipCode);
			}
		}

		// Query for related data in SF
		contractMap = new Deeded_ContractSelector().selectMapByName(contractCodes);
		weekMap = new WeekSelector().selectMapByCode(weekCodes);
		sectionMap = new Unit_SectionSelector().selectMapBySearchCode(sectionCodes);
		usageMap = new Usage_TypeSelector().selectMapByCode(usageCodes);
		ownershipMap = new Resort_OwnershipSelector().selectMapByCode(ownershipCodes);

		// Scatter SF data to each incoming record
		for (ImportedContractRecord xlData : xlRows) {
			if (contractMap.get(xlData.ContractCode) != null) {
				xlData.DeededContractId = contractMap.get(xlData.ContractCode).Id;
			}
			if (sectionMap.get(xlData.SectionCode) != null) {
				xlData.UnitICN = sectionMap.get(xlData.SectionCode).UnitId__r.ICN_Code__c;
				xlData.SectionICN = sectionMap.get(xlData.SectionCode).ICN_Code__c;
			}
			if (weekMap.get(xlData.WeekCode) != null) {
				xlData.WeekICN = weekMap.get(xlData.WeekCode).ICN_Code__c;
			}
			if (usageMap.get(xlData.UsageCode) != null) {
				xlData.UsageICN = usageMap.get(xlData.UsageCode).ICN_Code__c;
			}
			if (ownershipMap.get(xlData.ResortOwnershipCode) != null) {
				xlData.ResortOwnershipId = ownershipMap.get(xlData.ResortOwnershipCode).Id;
			}
			// Build out the full Unit Inventory ICN code if no elements are null
			if (xlData.ResortCode != null && xlData.UnitICN != null && xlData.WeekICN != null && xlData.SectionICN != null && xlData.UsageICN != null) {
				xlData.UnitInventoryICN = xlData.ResortCode + xlData.UnitICN + xlData.WeekICN + xlData.SectionICN + xlData.UsageICN;
				inventoryCodes.add(xlData.UnitInventoryICN);
			} else {
				xlData.UnitInventoryICN = '';
			}
		}
		// Query for Unit Inventory records by ICN code
		inventoryMap = new Unit_InventorySelector().selectMapByICN(inventoryCodes);
		for (ImportedContractRecord xlData : xlRows) {
			if (inventoryMap.get(xlData.UnitInventoryICN) != null) {
				xlData.UnitInventoryId = inventoryMap.get(xlData.UnitInventoryICN).Id;
				xlData.UnitInventoryICNOccupied = inventoryMap.get(xlData.UnitInventoryICN).Occupied__c;
			}
		}
		return xlRows;
	}

	/*
	  Populate the Batch Header and Batch Detail tables with spreadsheet data
	 */
	public static Map<String, Object> loadBatchData(List<Deeded_Batch_Detail__c> dataRows, List<Deeded_Batch_Detail_Owner__c> ownerRows, string batchType, string batchId, string developerId) {
		Map<String, Object> retMap = new Map<String, Object> ();
		retMap.put('errors', new Map<String, Object> ());
		retMap.put('records', new Map<String, Object> ());
		Savepoint sp = Database.setSavepoint();

		Id batchHeaderRecordType;
		Id batchRecordType;

		Deeded_Batch_Header__c header = new Deeded_Batch_Header__c();
		//pbh.RecordTypeId = batchHeaderRecordType;
		header.Name = batchId;
		header.Batch__c = batchId;
		header.Batch_Date__c = Date.today();
		header.Employee__c = UserInfo.getUserId();
		header.Developer__c = developerId;

		try {
			database.insert(header, true);
			((Map<String, Object>) retMap.get('records')).put('Deeded_Batch_Header__c', header.Id);
		} catch(Exception e) {
			Database.rollback(sp);
			((Map<String, Object>) retMap.get('errors')).put('Deeded_Batch_Header__c', e.getMessage());
			return retMap;
		}
		system.debug('retMap: ' + retmap);
		system.debug('dataRows: ' + dataRows);
		// batch details raw data insert to archive table
		for (deeded_batch_detail__c detail : datarows) {
			//detail.recordtypeid = batchrecordtype;
			detail.deeded_batch_header__c = header.id;
		}
		try {
			database.insert(datarows, true);
			((map<string, object>) retmap.get('records')).put('deeded_batch_detail__c', datarows.size());
		} catch(exception e) {
			database.rollback(sp);
			((map<string, object>) retmap.get('errors')).put('deeded_batch_detail__c', e.getmessage());
			return retmap;
		}

		if (batchtype == 'GrandPacific') {
			processGrandPacificBatchData(datarows, ownerRows, batchid, developerId, retmap, sp);
		}

		return retmap;

	}

	/*
	  For Grand Pacific resorts, insert Contracts, Documents and Owners
	 */
	public static void processGrandPacificBatchData(List<Deeded_Batch_Detail__c> batchDetailRows, List<Deeded_Batch_Detail_Owner__c> ownerRows, string batchId, string developerId, Map<String, Object> retMap, Savepoint sp) {
		Map<Integer, Deeded_Contract__c> contractMap = new Map<Integer, Deeded_Contract__c> ();
		Map<Integer, Map<String, Deeded_Document__c>> documentMap = new Map<Integer, Map<String, Deeded_Document__c>> ();
		Map<Integer, List<Deeded_Owner__c>> ownerMap = new Map<Integer, List<Deeded_Owner__c>> ();
		Map<Integer, List<Deeded_Owner_List__c>> ownerListMap = new Map<Integer, List<Deeded_Owner_List__c>> ();
		Map<String, Tag_Sheet__c> tagSheetMap = new Map<String, Tag_Sheet__c> (); // Map Key = batchNumber
		Set<String> batchNumbers = new Set<String> ();
		Id batchHeaderId;

		//Id tagSheetId = new Tag_SheetSelector().selectIdByBatchName(batchId);
		integer i = 1;
		Id contractRecordType = sObjectType.Deeded_Contract__c.RecordTypeInfosByName.get('Grand Pacific').RecordTypeId;
		// Insert Contracts
		for (Deeded_Batch_Detail__c batchDetailRow : batchDetailRows) {
			// If the row was not validated successfully, do not load the transactions
			if (batchDetailRow.Verified__c == false) { continue; }
			// Contract
			Deeded_Contract__c contract = new Deeded_Contract__c();
			contract.Name = batchDetailRow.Name;
			contract.Deeded_Batch_DetailId__c = batchDetailRow.Id;
			contract.Deeded_BatchId__c = batchDetailRow.Deeded_Batch_Header__c;
			contract.Unit_InventoryId__c = batchDetailRow.Unit_InventoryId__c;
			contract.ResortId__c = batchDetailRow.ResortId__c;
			contract.Resort_OwnershipId__c = batchDetailRow.Resort_OwnershipId__c;
			contract.Active__c = true;
			contract.Contract_Number__c = batchDetailRow.ContractId__c;
			contract.Contract_Code__c = batchDetailRow.Contract__c;
			contract.Search_Code__c = batchDetailRow.Unit_Inventory_ICN__c;
			contract.RecordTypeId = contractRecordType;
			contract.Contract_Price__c = batchDetailRow.ContractPrice__c;
			contract.Financed_Amount__c = batchDetailRow.Financed__c;
			contract.Down_Payment__c = batchDetailRow.DownPayment__c;
			contract.Closing_Cost__c = batchDetailRow.ClosingCost__c;
			contractMap.put(i, contract);
			i++;
			// Identify distinct Batch Numbers
			batchNumbers.add(batchDetailRow.BatchNumber__c);
			batchHeaderId = batchDetailRow.Deeded_Batch_Header__c;
		}
		try {
			Database.SaveResult[] srContracts = Database.insert(contractMap.values(), true);
			debugDatabaseResult(srContracts, 'Deeded_Contract__c');
			((Map<String, Object>) retMap.get('records')).put('Deeded_Contract__c', contractMap.size());
		} catch(Exception e) {
			Database.rollback(sp);
			((Map<String, Object>) retMap.get('errors')).put('Deeded_Contract__c', e.getMessage());
			return;
		}
		for (string batchNumber : batchNumbers) {
			Tag_Sheet__c tagSheet = new Tag_Sheet__c();
			//tagSheet.Order_Number__c = batchNumber;
			tagSheet.CTT_Order_Number__c = batchNumber;
			tagSheet.Deeded_Batch_HeaderId__c = batchHeaderId;
			tagSheetMap.put(batchNumber, tagSheet);
		}
		try {
			Database.SaveResult[] srTagSheets = Database.insert(tagSheetMap.values(), true);
			debugDatabaseResult(srTagSheets, 'Tag_Sheet_c');
			((Map<String, Object>) retMap.get('records')).put('Tag_Sheet_c', contractMap.size());
		} catch(Exception e) {
			Database.rollback(sp);
			((Map<String, Object>) retMap.get('errors')).put('Tag_Sheet_c', e.getMessage());
			return;
		}

		// Insert Documents 
		Set<String> documentTypeNames = new Set<String> { 'Grant Deed', 'Deed of Trust', 'Assignment' };
		Map<String, Deeded_Document_Type__c> documentTypeMap = new Deeded_Document_TypeSelector().selectMapByName(documentTypeNames);
		i = 1;
		for (Deeded_Batch_Detail__c row : batchDetailRows) {
			if (row.Verified__c == false) { continue; }
			Map<String, Deeded_Document__c> documentMapValue = new Map<String, Deeded_Document__c> ();
			for (string docType : documentTypeMap.keySet()) {
				Deeded_Document__c document = new Deeded_Document__c();
				document.Deeded_Contract__c = contractMap.get(i).Id;
				document.Name = docType;
				document.Deeded_Document_Type__c = documentTypeMap.get(docType).Id;
				document.Tag_SheetId__c = tagSheetMap.get(row.BatchNumber__c).Id;

				documentMapValue.put(docType, document);
			}
			documentMap.put(i, documentMapValue);
			i++;
		}
		try {
			List<Deeded_Document__c> allDocuments = new List<Deeded_Document__c> ();
			for (Map<String, Deeded_Document__c> documentMapValue : documentMap.values())
			{
				allDocuments.addAll(documentMapValue.values());
			}

			Database.SaveResult[] srDocuments = Database.insert(allDocuments, true);
			debugDatabaseResult(srDocuments, 'Deeded_Document__c');
			((Map<String, Object>) retMap.get('records')).put('Deeded_Document__c', allDocuments.size());
		} catch(Exception e) {
			Database.rollback(sp);
			((Map<String, Object>) retMap.get('errors')).put('Deeded_Document__c', e.getMessage());
			return;
		}
		// Insert Owners
		i = 1;
		Id defaultOwnerTypeId = new Deeded_Owner_TypeSelector().selectDefault();
		for (Deeded_Batch_Detail__c row : batchDetailRows) {
			// If the row was not validated successfully, do not load the transactions
			if (row.Verified__c == false) { continue; }
			// Owners will be loaded but currently it does not save the ordinal values (which is owner 1, 2, 3 etc)
			List<Deeded_Owner__c> ownerMapValues = new List<Deeded_Owner__c> ();
			for (Deeded_Batch_Detail_Owner__c ownerRow : ownerRows) {
				if (ownerRow.Import_Batch_Row__c.toPlainString() == row.Import_Batch_Row__c.toPlainString()) {
					Deeded_Owner__c deededOwner = new Deeded_Owner__c();
					string first = ownerRow.First_Name__c;
					string middle = ownerRow.Middle_Name__c;
					string last = ownerRow.Last_Name__c + (ownerRow.Suffix__c != '' ? (ownerRow.Last_Name__c != '' ? ', ' + ownerRow.Suffix__c : ' ') : '');
					deededOwner.Name = (first + (first.length() > 0 ? ' ' : '') + middle + (middle.length() > 0 ? ' ' : '') + last).trim(); // First + Middle + Last
					deededOwner.Title__c = ownerRow.Title__c;
					deededOwner.First_Name__c = first;
					deededOwner.Middle_Name__c = middle;
					deededOwner.Last_Name__c = last;
					deededOwner.Deeded_Owner_TypeId__c = defaultOwnerTypeId;
					ownerMapValues.add(deededOwner);
				}
			}

			ownerMap.put(i, ownerMapValues);
			i++;
		}
		try {
			List<Deeded_Owner__c> allOwners = new List<Deeded_Owner__c> ();
			for (List<Deeded_Owner__c> ownerList : ownerMap.values())
			{
				allOwners.addAll(ownerList);
			}

			Database.SaveResult[] srOwners = Database.insert(allOwners, true);
			debugDatabaseResult(srOwners, 'Deeded_Owner__c');
			((Map<String, Object>) retMap.get('records')).put('Deeded_Owner__c', allOwners.size());
		} catch(Exception e) {
			Database.rollback(sp);
			((Map<String, Object>) retMap.get('errors')).put('Deeded_Owner__c', e.getMessage());
			return;
		}
		// Insert Owner List junction (Deeded_Owner_List__c)
		i = 1;
		for (Deeded_Batch_Detail__c row : batchDetailRows) {
			// If the row was not validated successfully, do not load the transactions
			if (row.Verified__c == false) { continue; }
			// Get the Document that is the Grand Deed for each Contract
			// Get the Owners for that Contract
			// Add a new OwnerList record for each Owner/Document combination
			// Owner List
			Deeded_Document__c grantDeed = documentMap.get(i).get('Grant Deed');
			List<Deeded_Owner__c> ownerMapValue = ownerMap.get(i);
			List<Deeded_Owner_List__c> newOL = new List<Deeded_Owner_List__c> ();
			for (Deeded_Owner__c owner : ownerMapValue) {
				Deeded_Owner_List__c newOwnerListRecord = new Deeded_Owner_List__c();
				newOwnerListRecord.Deeded_Owner__c = owner.Id;
				newOwnerListRecord.Deeded_Document__c = grantDeed.Id;
				newOwnerListRecord.Deeded_Contract__c = contractMap.get(i).Id;
				newOL.add(newOwnerListRecord);
			}
			ownerListMap.put(i, newOL);
			i++;
		}
		try {
			List<Deeded_Owner_List__c> allOwnerLists = new List<Deeded_Owner_List__c> ();
			for (List<Deeded_Owner_List__c> ownerListMapValue : ownerListMap.values())
			{
				allOwnerLists.addAll(ownerListMapValue);
			}
			Database.SaveResult[] srOwnerLists = Database.insert(allOwnerLists, true);
			debugDatabaseResult(srOwnerLists, 'Deeded_Owner_List__c');
			((Map<String, Object>) retMap.get('records')).put('Deeded_Owner_List__c', allOwnerLists.size());
		} catch(Exception e) {
			Database.rollback(sp);
			((Map<String, Object>) retMap.get('errors')).put('Deeded_Owner_List__c', e.getMessage());
			return;
		}



	}



	public static void debugDatabaseResult(Database.SaveResult[] saveResults, string objectName) {
		for (Database.SaveResult sr : saveResults) {
			if (sr.isSuccess()) {
				// Operation was successful, so get the ID of the record that was processed
				System.debug('Successfully inserted ' + objectName + '. Id: ' + sr.getId());
			}
			else {
				// Operation failed, so get all errors
				for (Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug(objectName + ' fields that affected this error: ' + err.getFields());
				}
			}
		}

	}
	public static void debugDatabaseUpsertResult(Database.UpsertResult[] saveResults, string objectName) {
		for (Database.UpsertResult sr : saveResults) {
			if (sr.isSuccess()) {
				// Operation was successful, so get the ID of the record that was processed
				System.debug('Successfully inserted ' + objectName + '. Id: ' + sr.getId());
			}
			else {
				// Operation failed, so get all errors
				for (Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug(objectName + ' fields that affected this error: ' + err.getFields());
				}
			}
		}

	}

	// Wipes out Unit Inventory Contracts for the given contracts and recreates based on current data.
	// Also sets the Active Contract field on the Unit Inventory.  If contract is active, it sets it to
	// the contract id, else it nulls it out.
	public static void updateUnitInventoryContract(List<Deeded_Contract__c> contracts) {

		// Use a map in case there are more multiple contracts in the dataset that are referring to the same unit inventory.
		// This could happen in a historical data load where inactive contracts are being loaded against the same unit inventory, 
		// possibly because the unit have gone through multiple owners in the past.
		Map<Id, Unit_Inventory__c> uisToUpdate = new Map<Id, Unit_Inventory__c> ();
		List<Unit_Inventory_Contract__c> uicsToInsert = new List<Unit_Inventory_Contract__c> ();

		// We have to Requery the contract to get the Overlaps With field from Unit Inventory.  Overlaps With is a text area field
		// and so could not be surfaced up to Deeded Contract as a formula.
		for (Deeded_Contract__c contract :[SELECT Id, Active__c, Unit_InventoryId__c, Unit_InventoryId__r.Overlaps_With__c
		     FROM Deeded_Contract__c
		     WHERE Id IN :contracts]) {
			if (contract.Active__c) {
				// Parse the individual Unit Inventory Ids in Overlaps With.  We'll create a Unit Inventory Contract record
				// for every overlapping inventory id.
				if (String.isNotBlank(contract.Unit_InventoryId__r.Overlaps_With__c)) {
					for (String uid : contract.Unit_InventoryId__r.Overlaps_With__c.split(',')) {
						Unit_Inventory_Contract__c uic = new Unit_Inventory_Contract__c(
						                                                                Contract__c = contract.Id, Unit_Inventory__c = uid);
						uicsToInsert.add(uic);
					}

				}
				if (contract.Unit_InventoryId__c != null) {
					uisToUpdate.put(contract.Unit_InventoryId__c, new Unit_Inventory__c(Id = contract.Unit_InventoryId__c, Active_Contract__c = contract.Id));
				}
			} else {
				if (contract.Unit_InventoryId__c != null) {
					uisToUpdate.put(contract.Unit_InventoryId__c, new Unit_Inventory__c(Id = contract.Unit_InventoryId__c, Active_Contract__c = null));
				}
			}
		}

		// Hard delete all existing unit inventory contract records for the incoming contracts so we don't have to worry about trueing up.
		// We also need to do this for inactive contracts.  So we'll just do it for all.
		List<Unit_Inventory_Contract__c> uicsToDelete = [SELECT Id FROM Unit_Inventory_Contract__c WHERE Contract__c IN :contracts];
		if (uicsToDelete.size() > 0) {
			delete uicsToDelete;
			Database.emptyRecycleBin(uicsToDelete);
		}

		if (uicsToInsert.size() > 0) {
			insert uicsToInsert;
		}

		if (uisToUpdate.size() > 0) {
			update uisToUpdate.values();
		}

	}

	// Creates a title policy record for each contract inserted
	public static void createTitlePolicies(List<Deeded_Contract__c> contracts) {
		List<Title_Policy__c> titlePolicies = new List<Title_Policy__c> ();

		for (Deeded_Contract__c c : contracts) {
			titlePolicies.add(createTitlePolicyFromContract(c));
		}

		insert titlePolicies;
	}

	public static Title_Policy__c createTitlePolicyFromContract(Deeded_Contract__c c) {
		Title_Policy__c tp = new Title_Policy__c(
		                                         Name = c.Contract_Number__c,
		                                         Deeded_Contract__c = c.Id,
		                                         Contract_Sales_Price__c = c.Contract_Price__c,
		                                         Deposit__c = c.Down_Payment__c,
		                                         New_Loan_to_Seller__c = c.Financed_Amount__c,
		                                         Settlement_Fee_to_CTC__c = c.Closing_Cost__c
		);
		return tp;
	}

	public static void archiveTitlePolicy(Id contractId) {
		Title_Policy__c tp = new Title_PolicySelector().selectByContractNotArchived(contractId);
		if (tp != null) {
			Title_Policy__c tpNew = tp.clone(false, true, false, false);
			List<Title_Policy__c> titlePolicies = [select Id, Deeded_Contract__c, Is_Archived__c from Title_Policy__c where Is_Archived__c = false and Deeded_Contract__c = :contractId];
			for (Title_Policy__c titlePolicy : titlePolicies) {
				titlePolicy.Is_Archived__c = true;
			}
			update titlePolicies;
			insert tpNew;
		}
	}


	public class ImportedContractRecord {
		public string Contract { get; set; }
		public string ContractId { get; set; }
		public string ResortName { get; set; }
		public string RowNumber { get; set; }
		public string inventory_usage_type1 { get; set; }
		public string tu_unit_number1 { get; set; }
		public string wk_number1 { get; set; }
		public string Ownership { get; set; }

		public string DeveloperId { get; set; }
		public string ResortId { get; set; }
		public string UnitInventoryId { get; set; }
		public string DeededContractId { get; set; }
		public string ResortOwnershipId { get; set; }

		public string ResortCode { get; set; }
		//public string UnitCode {get; set;}
		public string WeekCode { get; set; }
		public string SectionCode { get; set; }
		public string UsageCode { get; set; }
		public string ContractCode { get; set; }
		public string ResortOwnershipCode { get; set; }


		public string ResortICN { get; set; }
		public string UnitICN { get; set; }
		public string WeekICN { get; set; }
		public string SectionICN { get; set; }
		public string UsageICN { get; set; }
		public string UnitInventoryICN { get; set; }
		public boolean UnitInventoryICNOccupied { get; set; }

	}

}