public with sharing class Title_PolicySelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Title_Policy__c.Id,
			Title_Policy__c.Name
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Title_Policy__c.sObjectType;
	}

	public List<Title_Policy__c> selectById(Set<ID> idSet)
	{
		return (List<Title_Policy__c>) selectSObjectsById(idSet);
	}

	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Title_Policy__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Title_Policy__c>) Database.query(query.toSOQL());
	}

	public Title_Policy__c selectByContractNotArchived(Id deededContractId) {
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Deeded_Contract__c = :deededContractId AND Is_Archived__c = false');
		// select ALL fields
		query.selectFields(Title_Policy__c.sObjectType.getDescribe().Fields.getMap().values());
		query.setLimit(1);
		List<Title_Policy__c> results =  Database.query(query.toSOQL());
		return results.size() == 1 ? results[0] : null;
	}

}