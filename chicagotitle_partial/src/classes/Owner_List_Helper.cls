/**
 * Created by bob.roos on 2/16/2017.
 */

public without sharing class Owner_List_Helper {
	/*
		Called before insert and update to set the Owner_List__c.Contract_c field value
	*/
    public static void setContractIdFromTransaction(List<Owner_List__c> newList) {
        if(!TriggerHelper.doExecute('Owner_List__c.setContractId')){ return; }
        List<Id> transIds = new List<Id>();
        for(Owner_List__c ownerList : newList) {
            transIds.add(ownerList.Transaction__c);
        }
        Map<Id,Transaction__c> transMap = new Map<Id,Transaction__c>([select Id, ContractId__c from Transaction__c WHERE Id in :transIds]);

        for(Owner_List__c ownerList : newList) {
            if (ownerList.Transaction__c != null) {
                if (transMap.containsKey(ownerList.Transaction__c)) {
                    ownerList.Contract__c = transMap.get(ownerList.Transaction__c).ContractId__c;
                }
            }
        }
    }
	/*
		Called by After Insert, Update, Delete triggers to set the grandparent Contract First_Current_Owner__c field value
		
	*/
	public static void updateContractRecord(List<Owner_List__c> newList) {
		if(!TriggerHelper.doExecute('Owner_List__c.updateContractRecord')){ return; }
		// Set of Contract Ids ( loop over Owner Transaction list)
		Set<Id> contractIds = new Set<Id>();
		for (Owner_List__c ol : newList) {
			contractIds.add(ol.Contract__c);
		}
		
		// List of latest owner transaction ids ( Latest Owner Transaction SOQL, loop over list )
		// TODO:  Validate sorting by CreatedDate does not cause problems with imported data.
		List<Id> latestOwnerTransactionList = new List<Id>();
		List<Contract__c> latestTransactions = [
			Select Id, Name, 
				(Select Id 
					from transactions__r 
					where Transaction_Type__r.Ownership__c = true
					ORDER BY closing_Date__c desc, name desc, createddate desc	
					limit 1
				)
			from contract__c 
			where id in :contractIds
		];
		for (Contract__c contract : latestTransactions ) {
			if (contract.Transactions__r.size() > 0 ) {
				latestOwnerTransactionList.add(contract.Transactions__r[0].Id);
			}
		}

		// Map of  Contract Id, First Owner Id ( first owner SOQL, loop over list )
		Map<Id, Id> firstOwnerMap = new Map<Id, Id>();
		List<Transaction__c> firstOwnerLookup = [
			select Id, Name, ContractId__c,   
				(Select Id, Owner__c
				from CTT_Owner_Lists__r 
				order by Transaction__c, Owner__r.CreatedDate
				limit 1)
			from Transaction__c 
			where id in  :latestOwnerTransactionList
		];
		for (Transaction__c tran : firstOwnerLookup ) {
			if (tran.CTT_Owner_Lists__r.size() > 0 ) {
				firstOwnerMap.put(tran.ContractId__c, tran.CTT_Owner_Lists__r[0].Owner__c  );
				
			}
		}

		List<Contract__c> contracts = [ SELECT Id, Name, First_Current_Owner__c from Contract__c where Id in :contractIds ];
		for (Contract__c contract : contracts) {
			contract.First_Current_Owner__c = firstOwnerMap.get(contract.id);
		}
		database.update(contracts);

	}

	public static void setCurrentOwners(List<Owner_List__c> newList) {
		Set<Id> contractIdList = new Set<Id>();
		for (Owner_List__c ol : newList) {
			contractIdList.add(ol.Contract__c);
		}
		Contract_Helper.setCurrentOwners(contractIdList);
	}


}