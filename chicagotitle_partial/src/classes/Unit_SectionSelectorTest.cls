@isTest
private class Unit_SectionSelectorTest {

	@isTest
	private static void testSelector() {

		Unit_Section__c record = new Unit_Section__c(Name = 'test');
		insert record;

		Set<Id> idSet = new Set<Id> {record.Id};
		List<String> nameList = new List<String> {'test'};
		Set<String> nameSet = new Set<String> {'test'};
		Set<String> codeSet = new Set<String>();

		Unit_SectionSelector selector = new Unit_SectionSelector();
		
		List<Unit_Section__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Unit_Section__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 1);
		
		Map<String, Unit_Section__c> mapByName = selector.selectMapBySearchCode(codeSet);
		System.assert(mapByName.size() == 0);

	}
}