public class PointsContractFunctionCancelController {
	
	private ApexPages.StandardController stdController;
	private Transaction__c trans;
	public Contract__c contract {get; set;}
	public List<wrapPointsContract> relatedContractList {get; set;}
	public Boolean isApplyToAllContracts {get; set;}
	public string membershipNumber {get; set;}
	public string contractNumber {get; set;}

	public PointsContractFunctionCancelController(ApexPages.StandardController stdController) {
		this.stdController = stdController;
		this.trans = (Transaction__c)stdController.getRecord();
		trans.ContractId__c = ApexPages.currentPage().getParameters().get('contractId');
		contract = [select id, name, Membership_Number__c, Membership_Number__r.Developer__c, First_Current_Owner__c from Contract__c where id = :trans.ContractId__c];
		relatedContractList = new List<wrapPointsContract>();
		isApplyToAllContracts = false;
		List<Contract__c> contracts = Contract_Helper.selectRelatedContracts(trans.ContractId__c);
		for (Contract__c contract : contracts) {
			relatedContractList.add(new wrapPointsContract(contract));
			isApplyToAllContracts = true;
		}
	}

	public PageReference saveAndReturn()
	{
		Contract_Helper.newCancelTransaction(trans.ContractId__c, trans.Closing_Date__c, trans.Comments__c, relatedContractList);
		PageReference parentPage = new PageReference('/' + contract.Id);
		parentPage.setRedirect(true);
		return parentPage;
	}

}