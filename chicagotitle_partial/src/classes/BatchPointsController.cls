public class BatchPointsController {
	public string inputType { get; set; }
	public string inputData { get; set; }
	public Date fDate { get; set; }
	
	public BatchPointsController() {
		fDate = Date.today();
	}

	@RemoteAction
	public static Map<String, List<SObject>> getWelkValidationData(List<string> contractNumberList, List<String> lenderList, List<String> resortCodeList) {
		//truncateTestData('Welk');
		Map<String, List<SObject>> outputMap = new Map<String, List<SObject>> ();
		// ResortCodeList - simple lookup of full resort name
		List<Resort__c> resorts = [select Id, Developer__c, Code__c, Name from resort__c where Code__c in :resortCodeList];
		outputMap.put('Resort__c', resorts);
		Id clubId = null;
		for (Resort__c resort : resorts) {
			if (resort.Developer__c != null) {
				clubId = resort.Developer__c;
				break;
			}
		}
		// Contract Number list (error if already exists)
		outputMap.put('Contract__c', [select Id, Contract_Number__c from Contract__c where Contract_Number__c in :contractNumberList and Resort__r.Developer__c = :clubId]);
		// Lender list (check that the Lender exists, error if not)
		outputMap.put('Lender__c', [Select Id, Developer__c, Name FROM Lender__c where Name in :lenderList and Developer__c = :clubId]);
		// Transaction Type
		outputMap.put('Transaction_Type__c', [select Id, Name, Developer__c from Transaction_Type__c where Name = 'Issued' and Developer__c = :clubId]);
		// Member Type
		outputMap.put('Member_Type__c', [select Id, Name from Member_Type__c where Name = 'Flex' and Developer__c = :clubId]);

		return outputMap;
	}

	@RemoteAction
	public static Map<String, List<SObject>> getShellValidationData(List<string> contractNumberList, List<String> lenderList, List<String> resortCodeList, List<String> expirationDateList, List<String> memberTypeList, List<String> membershipTypeList) {

		//truncateTestData('Shell');

		Map<String, List<SObject>> outputMap = new Map<String, List<SObject>> ();

		// ResortCodeList - simple lookup of full resort name
		List<Resort__c> resorts = [select Id, Developer__c, Code__c, Name from resort__c where Code__c in :resortCodeList];
		outputMap.put('Resort__c', resorts);
		Id clubId = null;
		for (Resort__c resort : resorts) {
			if (resort.Developer__c != null) {
				clubId = resort.Developer__c;
				break;
			}
		}
		// Contract Number list (error if already exists)
		outputMap.put('Contract__c', [select Id, Contract_Number__c from Contract__c where Contract_Number__c in :contractNumberList and Resort__r.Developer__c = :clubId]);
		// Lender list (check that the Lender exists, error if not)
		outputMap.put('Lender__c', [Select Id, Name FROM Lender__c where Name in :lenderList and Developer__c = :clubId]);
		// Member Type
		outputMap.put('Member_Type__c', [select Id, Name from Member_Type__c where Name in :memberTypeList and Developer__c = :clubId]);
		// MembershipType
		outputMap.put('Membership_Type__c', [select Id, Name from Membership_Type__c where Name in :membershipTypeList and Developer__c = :clubId]);
		// Expiration Date
		outputMap.put('Expiration_Date__c', [select Id, Name, Date_String__c, Expiration_Date__c from Expiration_Date__c where Date_String__c in :expirationDateList and Developer__c = :clubId]);
		outputMap.put('SelectExpirationDate', [select Date_String__c from Expiration_Date__c where Active__c = true and Developer__c = :clubId order by Expiration_Date__c]);

		// Transaction Type
		outputMap.put('Transaction_Type__c', [select Id, Name, Developer__c from Transaction_Type__c where Name = 'Issued' and Developer__c = :clubId]);

		System.debug(outputMap);
		return outputMap;
	}

	@RemoteAction
	public static Map<String, Object> loadData(List<Points_Batch_Closing__c> dataRows, List<Points_Batch_Detail_Owner__c> ownerRows, string batchType, string batchId, string clubId) {
		return Contract_Helper.loadData(dataRows, ownerRows, batchType, batchId, clubId);
	}

	





}