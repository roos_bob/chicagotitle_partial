/** 
* Deeded_Contract_AfterUpdate  Trigger Handler
*
* @author CRMCulture 
* @version 1.00
* @description  Class to handle Trigger handler
* @return void
*/
public class Deeded_Contract_AfterUpdate extends TriggerHandlerBase {
    /** 
    * mainEntry override
    *
    * @author CRMCulture
    * @date 20140701
    * @version 1.00
    * @description mainEntry override
    * @param tp Trigger Parameters construct
    * @return void
    */
    public override void mainEntry(TriggerParameters tp) {
        List<Deeded_Contract__c> contractsWithActiveChange = new List<Deeded_Contract__c>();

        for(Deeded_Contract__c newContract : (List<Deeded_Contract__c>)tp.newList) {
            Deeded_Contract__c oldContract = (Deeded_Contract__c)tp.oldMap.get(newContract.Id);

            // if active flag changes
            if (newContract.Active__c != oldContract.Active__c) contractsWithActiveChange.add(newContract);
        }

        // Delete Unit Inventory Contract join records when contract becomes inactive and 
        // clear the Active Contract lookup on the Unit Inventory on the contract.  If going from inactive
        // to active, create the unit inventory contract records.
        if (contractsWithActiveChange.size() > 0 && TriggerHelper.DoExecute('Deeded_Contract__c.UpdateUIC')) {
            Deeded_Contract_Helper.updateUnitInventoryContract(contractsWithActiveChange);
        }
    }
    /** 
    * @author CRMCulture
    * @version 1.00, 20140701
    * @description Called for the subsequent times in the same execution context. The trigger handlers can chose
    *               to ignore if they don't need the reentrant feature.
    * @param TriggerParameters The trigger parameters such as the list of records before and after the update.
    */
    //public override void inProgressEntry(TriggerParameters tp) {
        
    //}
}