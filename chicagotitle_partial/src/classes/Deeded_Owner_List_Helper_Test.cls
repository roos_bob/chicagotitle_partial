@isTest
private class  Deeded_Owner_List_Helper_Test {
	@testSetup static void setupData() {
	// Create Trigger Setting
		CS_Trigger_Setting__c setting = new CS_Trigger_Setting__c();
		setting.Name = 'Deeded_Owner_List__c.setLookups';
		setting.Enabled__c = true;
		insert setting;

	// Create Developer
		Map<String, Id> developerRecordTypes =  UtilityClass.getRecordTypeIdsByDeveloperName(Developer__c.SObjectType); 
		Id developerRTId;

		if(developerRecordTypes.containsKey('Deeded')){
			developerRTId = developerRecordTypes.get('Deeded');
		} else {
			System.assert(false, 'Missing Deeded record type for Developer__c');
		}

		Developer__c d = new Developer__c(Name = 'Grand Pacific', Active__c = true, RecordTypeId = developerRTId);
		insert d;

	// Create Resort
		Map<String, Id> resortRecordTypes =  UtilityClass.getRecordTypeIdsByDeveloperName(Resort__c.SObjectType); 
		Id resortRTId;

		if(resortRecordTypes.containsKey('Deeded')){
			resortRTId = resortRecordTypes.get('Deeded');
		} else {
			System.assert(false, 'Missing Deeded record type for Resort__c');
		}

		Resort__c r = new Resort__c(Name = 'Grand Pacific MarBrisa Resort', RecordTypeId = resortRTId, Developer__c = d.Id, Code__c = 'GMP');
		insert r;

	// Create Weeks
		Map<String, Week__c> wks = new Map<String, Week__c>();
		wks.put('01', new Week__c(Name = '01', ICN_Code__c = '01', Resort__c = r.Id, Season__c = 'Gold'));
		insert wks.values();

	// Create Usage Types
		Map<String, Usage_Type__c> uts = new Map<String, Usage_Type__c>();
		uts.put('Annual', new Usage_Type__c(Name = 'Annual', ICN_Code__c = 'Z', Resort__c = r.Id));
		uts.put('Biennial (Odd)', new Usage_Type__c(Name = 'Biennial (Odd)', ICN_Code__c = 'O', Resort__c = r.Id));
		uts.put('Biennial (Even)', new Usage_Type__c(Name = 'Biennial (Even)', ICN_Code__c = 'E', Resort__c = r.Id));
		insert uts.values();

	// Create Overlapping Usage Types
		List<Overlapping_Usage_Type__c> outs = new List<Overlapping_Usage_Type__c>();
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Annual').Id, Overlaps_WithId__c = uts.get('Biennial (Odd)').Id));
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Annual').Id, Overlaps_WithId__c = uts.get('Biennial (Even)').Id));
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Biennial (Odd)').Id, Overlaps_WithId__c = uts.get('Annual').Id));
		outs.add(new Overlapping_Usage_Type__c(Usage_TypeId__c = uts.get('Biennial (Even)').Id, Overlaps_WithId__c = uts.get('Annual').Id));
		insert outs;

	// Create Section Types
		Map<String, Section_Type__c> sts = new Map<String, Section_Type__c>();
		sts.put('Full', new Section_Type__c(Name = 'Full', ResortId__c = r.Id));
		sts.put('Bedroom', new Section_Type__c(Name = 'Bedroom', ResortId__c = r.Id));
		sts.put('Studio', new Section_Type__c(Name = 'Studio', ResortId__c = r.Id));
		insert sts.values();

	// Create Overlapping Section Types
		List<Overlapping_Section_Type__c> osts = new List<Overlapping_Section_Type__c>();
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Full').Id, Overlaps_WithId__c = sts.get('Bedroom').Id));
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Full').Id, Overlaps_WithId__c = sts.get('Studio').Id));
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Bedroom').Id, Overlaps_WithId__c = sts.get('Full').Id));
		osts.add(new Overlapping_Section_Type__c(Section_TypeId__c = sts.get('Studio').Id, Overlaps_WithId__c = sts.get('Full').Id));
		insert osts;

	// Create Unit
		Unit__c u = new Unit__c(Name = '5811', ResortId__c = r.Id, ICN_Code__c = '5811', Type__c = '2.7', View__c = 'Partk');
		insert u;

	// Create Unit Sections
		Map<String, Unit_Section__c> uss = new Map<String, Unit_Section__c>();
		uss.put('5811', new Unit_Section__c(Name = '5811', ICN_Code__c = 'A1', UnitId__c = u.Id, Size__c = '2BR/2BA', Section_TypeId__c = sts.get('Full').Id));
		uss.put('58111', new Unit_Section__c(Name = '58111', ICN_Code__c = 'B1', UnitId__c = u.Id, Size__c = '1BR/1BA', Section_TypeId__c = sts.get('Bedroom').Id));
		uss.put('58112', new Unit_Section__c(Name = '58112', ICN_Code__c = 'D1', UnitId__c = u.Id, Size__c = 'STUDIO/1BA', Section_TypeId__c = sts.get('Studio').Id));
		insert uss.values();

	// Create Unit Inventories
		Map<String, Unit_Inventory__c> uis = new Map<String, Unit_Inventory__c>();
		// Full, Even, Wk 1
		uis.put('Full/Even/Wk1', new Unit_Inventory__c(Name = 'GMP581101A1E', ResortId__c = r.Id, Unit_SectionId__c = uss.get('5811').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Even)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('5811').Id + ' ' + uts.get('Biennial (Even)').Id + ' ' + wks.get('01').Id));
		// Full, Odd, Wk 1
		uis.put('Full/Odd/Wk1', new Unit_Inventory__c(Name = 'GMP581101A1O', ResortId__c = r.Id, Unit_SectionId__c = uss.get('5811').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Odd)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('5811').Id + ' ' + uts.get('Biennial (Odd)').Id + ' ' + wks.get('01').Id));
		// Full, Annual, Wk 1
		uis.put('Full/Annual/Wk1', new Unit_Inventory__c(Name = 'GMP581101A1Z', ResortId__c = r.Id, Unit_SectionId__c = uss.get('5811').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Annual').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('5811').Id + ' ' + uts.get('Annual').Id + ' ' + wks.get('01').Id));

		// Bedroom, Even, Wk 1
		uis.put('Bedroom/Even/Wk1', new Unit_Inventory__c(Name = 'GMP581101B1E', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58111').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Even)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58111').Id + ' ' + uts.get('Biennial (Even)').Id + ' ' + wks.get('01').Id));
		// Bedroom, Odd, Wk 1
		uis.put('Bedroom/Odd/Wk1', new Unit_Inventory__c(Name = 'GMP581101B1O', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58111').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Odd)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58111').Id + ' ' + uts.get('Biennial (Odd)').Id + ' ' + wks.get('01').Id));
		// Bedroom, Annual, Wk 1
		uis.put('Bedroom/Annual/Wk1', new Unit_Inventory__c(Name = 'GMP581101B1Z', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58111').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Annual').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58111').Id + ' ' + uts.get('Annual').Id + ' ' + wks.get('01').Id));

		// Studio, Even, Wk 1
		uis.put('Studio/Even/Wk1', new Unit_Inventory__c(Name = 'GMP581101D1E', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58112').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Even)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58112').Id + ' ' + uts.get('Biennial (Even)').Id + ' ' + wks.get('01').Id));
		// Studio, Odd, Wk 1
		uis.put('Studio/Odd/Wk1', new Unit_Inventory__c(Name = 'GMP581101D1O', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58112').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Biennial (Odd)').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58112').Id + ' ' + uts.get('Biennial (Odd)').Id + ' ' + wks.get('01').Id));
		// Studio, Annual, Wk 1
		uis.put('Studio/Annual/Wk1', new Unit_Inventory__c(Name = 'GMP581101D1Z', ResortId__c = r.Id, Unit_SectionId__c = uss.get('58112').Id, 
			WeekId__c = wks.get('01').Id, Usage_TypeId__c = uts.get('Annual').Id, 
			Unique_Key__c = u.Id + ' ' + uss.get('58112').Id + ' ' + uts.get('Annual').Id + ' ' + wks.get('01').Id));

		insert uis.values();

		// Set overlapping ids.  
		uis.get('Full/Even/Wk1').Overlaps_With__c = uis.get('Full/Annual/Wk1').Id + ',' + uis.get('Bedroom/Annual/Wk1').Id + ',' +
			uis.get('Bedroom/Even/Wk1').Id + ',' + uis.get('Studio/Annual/Wk1').Id + ',' + uis.get('Studio/Even/Wk1').Id;

		uis.get('Full/Odd/Wk1').Overlaps_With__c = uis.get('Full/Annual/Wk1').Id + ',' + uis.get('Bedroom/Annual/Wk1').Id + ',' +
			uis.get('Bedroom/Odd/Wk1').Id + ',' + uis.get('Studio/Annual/Wk1').Id + ',' + uis.get('Studio/Odd/Wk1').Id;

		uis.get('Full/Annual/Wk1').Overlaps_With__c = uis.get('Full/Odd/Wk1').Id + ',' + uis.get('Full/Even/Wk1').Id + ',' + 
			uis.get('Bedroom/Annual/Wk1').Id + ',' + uis.get('Bedroom/Even/Wk1').Id + ',' + uis.get('Bedroom/Odd/Wk1').Id + ',' + 
			uis.get('Studio/Annual/Wk1').Id + ',' + uis.get('Studio/Even/Wk1').Id + ',' + uis.get('Studio/Odd/Wk1').Id;

		uis.get('Bedroom/Annual/Wk1').Overlaps_With__c = uis.get('Full/Odd/Wk1').Id + ',' + uis.get('Full/Even/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' +
			uis.get('Bedroom/Even/Wk1').Id + ',' + uis.get('Bedroom/Odd/Wk1').Id;

		uis.get('Bedroom/Even/Wk1').Overlaps_With__c = uis.get('Full/Even/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' +
			uis.get('Bedroom/Annual/Wk1').Id;

		uis.get('Bedroom/Odd/Wk1').Overlaps_With__c = uis.get('Full/Odd/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' +
			uis.get('Bedroom/Annual/Wk1').Id;

		uis.get('Studio/Annual/Wk1').Overlaps_With__c = uis.get('Full/Odd/Wk1').Id + ',' + uis.get('Full/Even/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' + 
			uis.get('Studio/Even/Wk1').Id + ',' + uis.get('Studio/Odd/Wk1').Id;

		uis.get('Studio/Even/Wk1').Overlaps_With__c = uis.get('Full/Even/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' +
			uis.get('Studio/Annual/Wk1').Id;

		uis.get('Studio/Odd/Wk1').Overlaps_With__c = uis.get('Full/Odd/Wk1').Id + ',' + uis.get('Full/Annual/Wk1').Id + ',' +
			uis.get('Studio/Annual/Wk1').Id;

		update uis.values();

		// Create contract
		Deeded_Contract__c c1 = new Deeded_Contract__c(Name = 'contract1', Unit_InventoryId__c = uis.get('Full/Even/Wk1').Id);
		insert c1;

		Deeded_Contract__c c2 = new Deeded_Contract__c(Name = 'contract2', Unit_InventoryId__c = uis.get('Full/Odd/Wk1').Id);
		insert c2;

		// Create documents
		Deeded_Document__c doc1 = new Deeded_Document__c(Name = 'doc1', Deeded_Contract__c = c1.Id);
		insert doc1;

		Deeded_Document__c doc2 = new Deeded_Document__c(Name = 'doc2', Deeded_Contract__c = c2.Id);
		insert doc2;

		// Create an owner
		Deeded_Owner__c o = new Deeded_Owner__c(Name = 'owner1');
		insert o;

	}

	@isTest static void testInsert() {
		Deeded_Document__c doc = [SELECT Id, Deeded_Contract__c, Deeded_Contract__r.Unit_InventoryId__c FROM Deeded_Document__c WHERE Name = 'doc1' LIMIT 1];
		Id ownerId = [SELECT Id FROM Deeded_Owner__c LIMIT 1].Id;

		Deeded_Owner_List__c ol = new Deeded_Owner_List__c(Deeded_Document__c = doc.Id, Deeded_Owner__c = ownerId);

		Test.startTest();
		insert ol;
		Test.stopTest();

		Deeded_Owner_List__c postOl = [SELECT Deeded_Contract__c, Unit_Inventory__c FROM Deeded_Owner_List__c WHERE Id = :ol.Id];

		System.assert(postOl.Deeded_Contract__c == doc.Deeded_Contract__c && postOl.Unit_Inventory__c == doc.Deeded_Contract__r.Unit_InventoryId__c,
			'Lookups were not set correctly');
	}

	// Not needed as Deeded_Document__c is not reparentable
	//@isTest static void testUpdate() {
	//	Deeded_Document__c doc1 = [SELECT Id, Deeded_Contract__c, Deeded_Contract__r.Unit_InventoryId__c FROM Deeded_Document__c WHERE Name = 'doc1' LIMIT 1];
	//	Id ownerId = [SELECT Id FROM Deeded_Owner__c LIMIT 1].Id;

	//	Deeded_Owner_List__c ol = new Deeded_Owner_List__c(Deeded_Document__c = doc1.Id, Deeded_Owner__c = ownerId);
	//	insert ol;

	//	Deeded_Document__c doc2 = [SELECT Id, Deeded_Contract__c, Deeded_Contract__r.Unit_InventoryId__c FROM Deeded_Document__c WHERE Name = 'doc2' LIMIT 1];

	//	ol.Deeded_Document__c = doc2.Id;

	//	Test.startTest();
	//	update ol;
	//	Test.stopTest();

	//	Deeded_Owner_List__c postOl = [SELECT Deeded_Contract__c, Unit_Inventory__c FROM Deeded_Owner_List__c WHERE Id = :ol.Id];

	//	System.assert(postOl.Deeded_Contract__c == doc2.Deeded_Contract__c && postOl.Unit_Inventory__c == doc2.Deeded_Contract__r.Unit_InventoryId__c,
	//		'Lookups were not set correctly');
	//}
}