public with sharing class ResortSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Resort__c.Id,
			Resort__c.Name,
			Resort__c.Developer__c,
			Resort__c.Code__c,
			Resort__c.Legacy_Id__c

		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Resort__c.sObjectType;
	}

	public List<Resort__c> selectById(Set<ID> idSet)
	{
		return (List<Resort__c>) selectSObjectsById(idSet);
	}
	
	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Resort__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Resort__c>) Database.query(query.toSOQL());
	}

	public List<Resort__c> selectByName(Set<String> nameSet)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameSet');
		return  (List<Resort__c>) Database.query(query.toSOQL());
	}

	public Map<String, Resort__c> selectMapByName(Set<String> names)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :names');
		List<Resort__c> records = (List<Resort__c>) Database.query(query.toSOQL());

		Map<String, Resort__c> resultMap = new Map<String, Resort__c>();
		for (Resort__c record: records) {
			resultMap.put(record.Name, record);
		}
		return resultMap;

	}


}