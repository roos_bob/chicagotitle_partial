@isTest
private class Deeded_Batch_HeaderSelectorTest {

	@isTest
	private static void testSelector() {
		Deeded_Batch_HeaderSelector selector = new Deeded_Batch_HeaderSelector();
		Set<Id> idSet = new Set<Id>();
		List<String> nameList = new List<String> {'test'};
		List<Deeded_Batch_Header__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 0);
		List<Deeded_Batch_Header__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 0);
		Id idByName = selector.selectIdByName('test');
		System.assert(idByName == null);

	}
}