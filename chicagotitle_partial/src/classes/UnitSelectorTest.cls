@isTest
private class UnitSelectorTest {

	@isTest
	private static void testSelector() {

		Unit__c record = new Unit__c(Name = 'test');
		insert record;

		UnitSelector selector = new UnitSelector();
		Set<Id> idSet = new Set<Id> {record.Id};
		List<String> nameList = new List<String> {'test'};
		Set<String> nameSet = new Set<String> {'test'};
		
		List<Unit__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Unit__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 1);
		
		List<Unit__c> byNameSet = selector.selectByName(nameSet);
		System.assert(byNameSet.size() == 1);


	}
}