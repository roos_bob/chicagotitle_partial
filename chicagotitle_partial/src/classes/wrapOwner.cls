public class wrapOwner {
	public Boolean selected {get; set;}
	public Owner__c owner {get; set;} 
	public Id contractId {get; set;} 
	public string contractName {get; set;} 
	public Boolean isDirty {get; set;}
	public wrapOwner(Owner__c o) {
		selected = false;
		owner = o;	
		isDirty = false;
	}
	public wrapOwner(Owner__c o, Id c, string cName) {
		selected = false;
		owner = o;	
		contractId = c;
		contractName = cName;
		isDirty = false;
	}
}