/** 
* Deeded_Document_AfterInsert  Trigger Handler
*
* @author Conga Services 
* @version 1.00
* @description  Class to handle Trigger handler
* @return void
*/
public class Deeded_Document_BeforeUpdate extends TriggerHandlerBase {
    /** 
    * mainEntry override
    *
    * @author Conga Services
    * @date 20171106
    * @version 1.00
    * @description mainEntry override
    * @param tp Trigger Parameters construct
    * @return void
    */
    public override void mainEntry(TriggerParameters tp) {
    	if (!TriggerHelper.DoExecute('Deeded_Document.SetDocType')) { return; }
   		Deeded_Document_Helper.updateDocumentType((Map<Id, Deeded_Document__c>)tp.newMap);

    }
    /** 
    * @author Conga Services
    * @version 1.00, 20171106
    * @description Called for the subsequent times in the same execution context. The trigger handlers can chose
    *               to ignore if they don't need the reentrant feature.
    * @param TriggerParameters The trigger parameters such as the list of records before and after the update.
    */
    //public override void inProgressEntry(TriggerParameters tp) {
        
    //}
}