public with sharing class Deeded_Batch_HeaderSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Deeded_Batch_Header__c.Id,
			Deeded_Batch_Header__c.Name,
			Deeded_Batch_Header__c.Batch__c
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Deeded_Batch_Header__c.sObjectType;
	}

	public List<Deeded_Batch_Header__c> selectById(Set<ID> idSet)
	{
		return (List<Deeded_Batch_Header__c>) selectSObjectsById(idSet);
	}

	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Deeded_Batch_Header__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Deeded_Batch_Header__c>) Database.query(query.toSOQL());
	}

	public Id selectIdByName(string batchName) {
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name = :batchName');
		Id objId = null;
		List<Deeded_Batch_Header__c> objs = Database.query(query.toSOQL());
		if (objs.size() > 0) objId = objs[0].Id;
		return objId;

	}

}