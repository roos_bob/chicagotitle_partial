/*
 * Developer: Bob Roos <bob.roos@getconga.com>
 * Description: 
 *    Provides functions related to Contract actions
 */

public without sharing class Contract_Helper {
	private static List<SObjectType> uowObjs = new List<SObjectType> { Contract__c.SObjectType, Transaction__c.SObjectType, Owner__c.SObjectType, Owner_List__c.SObjectType };

	private static Map<Id, Map<String, Id>> transactionTypes { get; set; } //Map<Developer Id, map<type name, type id> >

	private static void initializeTransactionTypes() {
		if (transactionTypes == null) {
			transactionTypes = new Map<Id, Map<String, Id>> ();
			List<Transaction_Type__c> types = [SELECT Id, Name, Developer__c FROM Transaction_Type__c];
			for (Transaction_Type__c type : types) {
				if (!transactionTypes.containsKey(type.Developer__c)) {
					transactionTypes.put(type.Developer__c, new Map<String, Id> ());
				}
				transactionTypes.get(type.Developer__c).put(type.Name, type.Id);
			}
		}
	}


	/*
	  Cancel function
	  Cancel the current contract
	  For the current Contract:
	  The current Contract is updated to set Points__c = 0
	  A new transaction is added to the current Contract.  Type = Cancelled
	  The owners associated with transaction Id in Contract__c.Last_Owner_Trans__c are associated with the new transaction
	  For Related Contracts:
	  The current Contract is updated to set Points__c = 0
	  A new transaction is added to the related Contract.  Type = Cancelled
	  The owners associated with transaction Id in Contract__c.Last_Owner_Trans__c are associated with the new transaction
	 
	 
	 */
	public static void newCancelTransaction(Id contractId, Date closingDate, string comments, List<wrapPointsContract> relatedContracts) {
		Contract__c sourceContract = new ContractSelector().getContract(contractId);
		if (sourceContract != null) {
			Id cancelledTransactionTypeId = selectTransactionTypeId('Cancelled', sourceContract.Membership_Number__r.Developer__c);
			List<Contract__c> contractsToUpdate = joinContracts(sourceContract, relatedContracts);
			Set<Id> contractLastTransactionSet = new Set<Id> ();
			contractLastTransactionSet.add(sourceContract.Last_Owner_Trans__c);
			if (relatedContracts != null) {
				for (wrapPointsContract relatedContractWrapper : relatedContracts) {
					if (relatedContractWrapper.selected) {
						contractLastTransactionSet.add(relatedContractWrapper.contract.Last_Owner_Trans__c);
					}
				}
			}
			// Get the owners related to each Contract/Last transaction
			List<Owner_List__c> owners = [SELECT Owner__c, transaction__c FROM Owner_List__c WHERE transaction__c in :contractLastTransactionSet];

			fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(uowObjs);
			for (Contract__c contract : contractsToUpdate) {
				contract.Points__c = 0;
				if (contract.id != null) { uow.registerDirty(contract); } else { uow.registerNew(contract); }
				Transaction__c trans = getNewTransaction(cancelledTransactionTypeId, closingDate, comments, 0);
				uow.registerNew(trans, Transaction__c.ContractId__c, contract);

				for (Owner_List__c owner : owners) {
					if (owner.transaction__c == contract.Last_Owner_Trans__c) {
						Owner_List__c ol = new Owner_List__c();
						ol.Owner__c = owner.Owner__c;
						uow.registerNew(ol, Owner_List__c.Transaction__c, trans);
					}
				}
			}
			uow.commitWork();
		}
	}

	/*
	  Split function
	  Take points from one contract and apply them to a newly created contract
	  Update the original Contract to set the new number of Points__c.
	  Create a new Contract associated to the specified Membership Number
	  Add a new Transaction to the original Contract (Split), specifying the old and new points and the related new Contract
	  Associate the owners from the Original Contract Last_Owner_Trans__c transaction with the Split transaction.
	  Add a new Transaction to the new Contract (Issued), specifying the new points and the related original contract
	  Add the new owners passed as parameters
	  Associate the new Owners with the Issued transaction
	 */
	public static void newSplitTransaction(Id contractId, string membershipNumber, string contractNumber, Date closingDate, String comments, Decimal oldPoints, Decimal newPoints, List<Owner__c> owners) {
		// Original Contract
		Contract__c originalContract = new ContractSelector().getContract(contractId);
		if (originalContract != null) {
			fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
			                                                          new List<SObjectType> { Membership_Number__c.SObjectType, Contract__c.SObjectType, Transaction__c.SObjectType, Owner__c.SObjectType, Owner_List__c.SObjectType }
			);
			originalContract.Points__c = oldPoints;
			uow.registerDirty(originalContract);
			//New Membership Number
			Membership_Number__c mn;
			List<Membership_Number__c> membershipNumbers = [
			                                                select Id from Membership_Number__c
			                                                WHERE Developer__c = :originalContract.Membership_Number__r.Developer__c
			                                                AND Name = :membershipNumber
			                                               ];
			if (membershipNumbers.size() > 0) {
				mn = membershipNumbers[0];
			} else {
				mn = new Membership_Number__c();
				mn.Name = membershipNumber;
				mn.Developer__c = originalContract.Membership_Number__r.Developer__c;
				uow.registerNew(mn);
			}
			// New Contract (cloned from original contract, linked to new membership number)
			Contract__c newContract = originalContract.clone(false, false, false, false);
			newContract.Name = contractNumber;
			newContract.Contract_Number__c = contractNumber;
			newContract.Points__c = newPoints;
			newContract.Batch_Number__c = null;
			newContract.Date_Signed__c = closingDate;
			uow.registerNew(newContract, Contract__c.Membership_Number__c, mn);

			// New Split Transaction on original contract
			Id splitTransactionTypeId = selectTransactionTypeId('Split', originalContract.Membership_Number__r.Developer__c);
			Transaction__c splitTrans = getNewTransaction(splitTransactionTypeId, closingDate, comments, newPoints, oldPoints);
			uow.registerNew(splitTrans, Transaction__c.Child_ContractId__c, newContract);
			uow.registerRelationship(splitTrans, Transaction__c.ContractId__c, originalContract);

			// New "Issued" transaction on new contract
			Id issuedTransactionTypeId = selectTransactionTypeId('Issued', originalContract.Membership_Number__r.Developer__c);
			Transaction__c issuedTrans = getNewTransaction(issuedTransactionTypeId, closingDate, comments, newPoints);
			uow.registerNew(issuedTrans, Transaction__c.ContractId__c, newContract);
			uow.registerRelationship(issuedTrans, Transaction__c.Child_ContractId__c, originalContract);

			//Split owners come from original transaction
			List<Owner_List__c> originalOwners = [SELECT Owner__c, transaction__c FROM Owner_List__c WHERE transaction__c = :originalContract.Last_Owner_Trans__c];
			for (Owner_List__c ownerList : originalOwners) {
				Owner_List__c ol = new Owner_List__c();
				ol.Owner__c = ownerList.Owner__c;
				uow.registerNew(ol, Owner_List__c.Transaction__c, splitTrans);
			}
			// Issued Owners -- these were added on the page by the user
			for (Owner__c newOwner : owners) {
				uow.registerNew(newOwner);
				Owner_List__c newOL = new Owner_List__c();
				uow.registerRelationship(newOL, Owner_List__c.Owner__c, newOwner);
				uow.registerNew(newOL, Owner_List__c.Transaction__c, issuedTrans);
			}
			uow.commitWork();
		}
	}

	/*
	  Transfer function
	  On the existing contract, remove ALL current owner, add all new owners		
	 */
	public static void newTransferTransaction(Id contractId, Date closingDate, string comments, List<Owner__c> owners, List<wrapPointsContract> relatedContracts) {
		Contract__c sourceContract = new ContractSelector().getContract(contractId);

		if (sourceContract != null) {
			Id transactionTypeId = selectTransactionTypeId('Transferred', sourceContract.Membership_Number__r.Developer__c);
			List<Contract__c> contractsToUpdate = joinContracts(sourceContract, relatedContracts);

			fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> { Contract__c.SObjectType, Transaction__c.SObjectType, Owner__c.SObjectType, Owner_List__c.SObjectType });
			for (Owner__c owner : owners) {
				if (owner.Id == null) { uow.registerNew(owner); }
			}
			for (Contract__c contract : contractsToUpdate) {
				if (contract.id == null) { uow.registerNew(contract); }
				Transaction__c newTrans = getNewTransaction(transactionTypeId, closingDate, comments, contract.Points__c);
				uow.registerNew(newTrans, Transaction__c.ContractId__c, contract);
				for (Owner__c owner : owners) {
					Owner_List__c ol = new Owner_List__c();
					uow.registerNew(ol, Owner_List__c.Owner__c, owner);
					uow.registerRelationship(ol, Owner_List__c.Transaction__c, newTrans);
				}
			}
			uow.commitWork();
		}
	}





	/*
	  Add/Remove function
	  Add new owners or remove existing owners to the existing contract
	  Related Contracts will end up having the identical owners as the source contract
	 */
	public static void newAddRemoveTransaction(Id contractId, Date closingDate, string comments, List<Owner__c> owners, List<wrapOwner> oldOwners, List<wrapPointsContract> relatedContracts) {
		Contract__c sourceContract = new ContractSelector().getContract(contractId);

		if (sourceContract != null) {
			Id transactionTypeId = selectTransactionTypeId('Add/Remove Owner', sourceContract.Membership_Number__r.Developer__c);
			List<Contract__c> contractsToUpdate = joinContracts(sourceContract, relatedContracts);

			// The new owners will be:  any new owners added + any old owners not removed
			for (wrapOwner oldOwner : oldOwners) {
				if (!oldOwner.selected) { // For the old owners, the user was prompted to select the ones to remove
					owners.add(oldOwner.owner); // old owners are added to the list of new owners
				}
			}

			fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(uowObjs);
			for (Owner__c owner : owners) {
				if (owner.Id == null) {
					uow.registerNew(owner);
				}
				else {
					uow.registerDirty(owner);
				}
			}
			for (Contract__c contract : contractsToUpdate) {
				//uow.registerDirty(contract);  // This prevented the trigger on Transaction from updating the Contract.
				Transaction__c newTrans = getNewTransaction(transactionTypeId, closingDate, comments, contract.Points__c);
				uow.registerNew(newTrans, Transaction__c.ContractId__c, contract);

				// ALL selected contracts will end with the exact same owners (if related contracts were selected)
				for (Owner__c owner : owners) {
					Owner_List__c ol = new Owner_List__c();
					uow.registerNew(ol, Owner_List__c.Owner__c, owner);
					uow.registerRelationship(ol, Owner_List__c.Transaction__c, newTrans);
				}
			}
			uow.commitWork();
		}
	}

	/*
	  Name Change function
	  Keep all current owners but apply a name change
	 */
	public static void newNameChangeTransaction(Id contractId, Date closingDate, string comments,
	                                            List<Owner__c> dirtyOwnerList, List<Owner__c> cleanOwnerList, List<wrapOwner> relatedOwnerList) {
		Contract__c sourceContract = new ContractSelector().getContract(contractId);
		if (sourceContract != null) {

			List<Owner__c> modifiedOwners = new List<Owner__c> ();
			List<Owner__c> unmodifiedOwners = new List<Owner__c> ();
			for (Owner__c o1 : dirtyOwnerList)
			{
				for (Owner__c o2 : cleanOwnerList)
				{
					if (o1.Id == o2.Id) {
						if (isOwnerDirty(o1, o2)) {
							modifiedOwners.add(o1);
						} else {
							unmodifiedOwners.add(o1);
						}
						break;
					}
				}
			}
			Id transactionTypeId = selectTransactionTypeId('Name Change', sourceContract.Membership_Number__r.Developer__c);
			fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(uowObjs);
			//uow.registerDirty(sourceContract);
			Transaction__c newTrans = getNewTransaction(transactionTypeId, closingDate, comments, sourceContract.Points__c);
			uow.registerNew(newTrans, Transaction__c.ContractId__c, sourceContract);

			// Modified Owners
			for (Owner__c owner : modifiedOwners) {
				owner.Id = null;
				uow.registerNew(owner);
				Owner_List__c ol = new Owner_List__c();
				uow.registerNew(ol, Owner_List__c.Owner__c, owner);
				uow.registerRelationship(ol, Owner_List__c.Transaction__c, newTrans);
			}
			// Unmodified Owners
			for (Owner__c owner : unmodifiedOwners) {
				Owner_List__c ol = new Owner_List__c();
				uow.registerNew(ol, Owner_List__c.Owner__c, owner);
				uow.registerRelationship(ol, Owner_List__c.Transaction__c, newTrans);
			}
			// Related owners
			// Need to add new transactions to the other related Contracts
			// ONLY NEED TO DO THIS IF A SINGLE OWNER NAME WAS CHANGED
			if (modifiedOwners.size() == 1) {
				Owner__c sourceOwner = modifiedOwners[0];
				// Populate a set of unique Contract Ids from the Selected related owners
				Set<Id> selectedContracts = new Set<Id> ();
				for (wrapOwner wo : relatedOwnerList) {
					if (wo.selected) {
						selectedContracts.add(wo.contractId);
					}
				}
				// This will pull all the possible related contracts
				Map<Id, Contract__c> ownerRelatedContracts = new Map<Id, Contract__c> (selectRelatedContracts(contractId));
				// Loop over each selected related contract
				for (Id relatedContractId : selectedContracts) {
					Contract__c relatedContract = ownerRelatedContracts.get(relatedContractId);
					// Create the new Transaction for the related Contract
					Transaction__c newRelatedTrans = getNewTransaction(transactionTypeId, closingDate, comments, relatedContract.Points__c);
					uow.registerNew(newRelatedTrans, Transaction__c.ContractId__c, relatedContract);
					for (wrapOwner wo : relatedOwnerList) {
						if (wo.contractId == relatedContractId) {
							Owner__c relatedOwner;
							// If the Owner was selected don't use it anymore.  Replace it with the modified Owner.
							if (wo.selected) {
								relatedOwner = sourceOwner.clone();
								uow.registerNew(relatedOwner);
							} else {
								relatedOwner = wo.owner;
							}
							Owner_List__c rol = new Owner_List__c();
							uow.registerNew(rol, Owner_List__c.Owner__c, relatedOwner);
							uow.registerRelationship(rol, Owner_List__c.Transaction__c, newRelatedTrans);
						}
					}
				}
			}
			uow.commitWork();
		}
	}

	/*
	  Save Owner List for transaction
	 */
	public static List<Owner_List__c> insertOwnerList(Id transactionId, List<Owner__c> owners) {
		List<Owner_List__c> ownerLists = new List<Owner_List__c> ();
		for (Owner__c owner : owners) {
			Owner_List__c ol = new Owner_List__c();
			ol.Owner__c = owner.Id;
			ol.Transaction__c = transactionId;
			ownerLists.add(ol);
		}
		Database.insert(ownerLists);
		return ownerLists;
	}

	/*
	  Override to create a new Transaction
	 */
	public static Transaction__c insertNewTransaction(string transactionType, Contract__c contract, Date closingDate, string comments, decimal oldPoints) {
		return insertNewTransaction(transactionType, contract, closingDate, comments, oldPoints, null, null);
	}

	/*
	  Override to create a new Transaction
	 */
	public static Transaction__c insertNewTransaction(string transactionType, Contract__c contract, Date closingDate, string comments, decimal oldPoints, decimal newPoints) {
		return insertNewTransaction(transactionType, contract, closingDate, comments, oldPoints, newPoints, null);
	}

	/*
	  Create a new Transaction
	 */
	public static Transaction__c insertNewTransaction(string transactionType, Contract__c contract, Date closingDate, string comments, decimal oldPoints, decimal newPoints, Id relatedContractId) {
		Id transactionTypeId = selectTransactionTypeId(transactionType, contract.Membership_Number__r.Developer__c);
		Transaction__c trans = new Transaction__c();
		trans.ContractId__c = contract.Id;
		trans.Transaction_Type__c = transactionTypeId;
		trans.Closing_Date__c = closingDate;
		trans.Comments__c = comments;
		trans.Employee__c = UserInfo.getUserId();
		trans.Entered__c = DateTime.now();
		trans.Points__c = oldPoints;
		trans.Split_Points__c = newPoints;
		trans.Child_ContractId__c = relatedContractId;
		Database.insert(trans);
		return trans;
	}

	/*
	  Override to create a new Transaction
	 */
	public static Transaction__c getNewTransaction(Id transactionType, Date closingDate, string comments, decimal points) {
		return getNewTransaction(transactionType, closingDate, comments, points, null);
	}


	/*
	  Create a new Transaction
	 */
	public static Transaction__c getNewTransaction(Id transactionTypeId, Date closingDate, string comments, decimal points, decimal splitPoints) {
		Transaction__c trans = new Transaction__c();
		trans.Transaction_Type__c = transactionTypeId;
		trans.Closing_Date__c = closingDate;
		trans.Comments__c = comments;
		trans.Employee__c = UserInfo.getUserId();
		trans.Entered__c = DateTime.now();
		trans.Points__c = points;
		trans.Split_Points__c = splitPoints;
		return trans;
	}

	/*
	  Query for the Transaction Type Id
	 */
	public static Id selectTransactionTypeId(string typeName, Id developerId) {
		initializeTransactionTypes();
		Id transTypeId = null;
		Map<String, Id> transTypes = transactionTypes.get(developerId);
		if (transTypes != null) {
			transTypeId = transTypes.get(typeName);
		}
		return transTypeId;
	}


	/*
	  Query for related Owner_List__c records
	 */
	public static List<Owner_List__c> selectOwnerList(Id transactionId) {
		List<Owner_List__c> ol = [
		                          SELECT id, name, Owner__c, Transaction__c FROM Owner_List__c WHERE transaction__c = :transactionId
		                         ];
		return ol;
	}

	/*
	  Query for related Owner__c records
	 */
	public static List<Owner__c> selectOwners(Id transactionId) {
		List<Owner__c> owners = [
		                         SELECT Id, Name, Salutation__c, First_Name__c, Middle_Name__c, Last_Name__c, Suffix__c, Trust__c, Street__c, City__c, State__c, Country__c, Postal_Code__c, Email__c, Phone__c, Developer__c
		                         FROM Owner__c
		                         WHERE Id in(SELECT Owner__c FROM Owner_List__c WHERE transaction__c = :transactionId)
		                        ];
		return owners;
	}

	public static List<wrapOwner> selectOwnersForMembership(Id membershipNumberId, Id originalContractId) {
		List<Transaction__c> transactions = [
		                                     SELECT Id, ContractId__c, ContractId__r.Name,
		(SELECT Id, Owner__c,
		 Owner__r.Name, Owner__r.Salutation__c, Owner__r.First_Name__c, Owner__r.Middle_Name__c,
		 Owner__r.Last_Name__c, Owner__r.Suffix__c, Owner__r.Trust__c,
		 Owner__r.Street__c, Owner__r.City__c, Owner__r.State__c,
		 Owner__r.Country__c, Owner__r.Postal_Code__c,
		 Owner__r.Email__c, Owner__r.Phone__c, Owner__r.Developer__c
		 FROM CTT_Owner_Lists__r)
		                                     FROM Transaction__c
		                                     WHERE Id in(select Last_Owner_Trans__c FROM Contract__c WHERE Membership_Number__c = :membershipNumberId AND Id != :originalContractId)
		                                    ];

		List<wrapOwner> retMap = new List<wrapOwner> ();
		for (Transaction__c trans : transactions) {
			for (Owner_List__c ol : trans.CTT_Owner_Lists__r) {
				retMap.add(new wrapOwner(ol.Owner__r, trans.ContractId__c, trans.ContractId__r.Name));
			}
		}
		return retMap; // Map<Contract Id, owners>
	}


	/*
	  Query for all Contracts related to the same Membership Number 
	 */
	public static List<Contract__c> selectRelatedContracts(Id contractId) {
		List<Contract__c> contracts = [select id, Membership_Number__c from Contract__c where Id = :contractId];
		Id membershipId = null;
		for (Contract__c contract : contracts) {
			membershipId = contract.Membership_Number__c;
		}
		List<Contract__c> relatedContracts = [
		                                      SELECT
		                                      Id, Name, Membership_Number__c, Membership_Number__r.Developer__c, First_Current_Owner__c
		                                      , Batch_Number__c, Contract_Number__c, Date_Signed__c, Expiration_Date__c, Lender__c
		                                      , Member_Type__c, Membership_Type__c, Points__c, Resort__c, Season__c
		                                      , Last_Owner_Trans__c, First_Current_Owner__r.Name, Active__c
		                                      from Contract__c
		                                      where Membership_Number__c = :membershipId and Id != :contractId
		                                     ];
		return relatedContracts;
	}





	public static void setCurrentOwners(Set<Id> contractIds) {
		fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(new List<SObjectType> { Contract__c.SObjectType, Owner_List__c.SObjectType });

		Map<Id, contractOwners> contractOwnerMap = new Map<Id, contractOwners> ();
		Map<Id, Contract__c> contracts = new ContractSelector().selectMapById(contractIds);
		if (contracts.size() == 0) return;
		List<Owner_List__c> ownerlist = new Owner_ListSelector().selectByContract(contractIds);
		if (ownerlist.size() == 0) return;

		for (Owner_List__c ol : ownerlist) {
			if (ol.Transaction__c == contracts.get(ol.Contract__c).Last_Owner_Trans__c) {
				ol.Current_Owner_of_Contract__c = ol.Contract__c;
				if (!contractOwnerMap.containsKey(ol.Contract__c)) {
					contractOwnerMap.put(ol.Contract__c, new contractOwners(ol.Owner__r.Name));
				} else {
					contractOwnerMap.get(ol.Contract__c).addOwner(ol.Owner__r.Name);
				}
			} else {
				ol.Current_Owner_of_Contract__c = null;
			}
			uow.registerDirty(ol);
		}
		for (Id contractId : contracts.keyset()) {
			Contract__c contract = contracts.get(contractId);
			if (contractOwnerMap.containsKey(contractId)) {
				contract.Owners__c = contractOwnerMap.get(contractId).contractOwners;
			} else {
				contract.Owners__c = null;
			}
			uow.registerDirty(contract);
		}
		uow.commitWork();
	}
	/*
	  Combines the source contract ane the list of related contracts into a single list of contracts
	 */
	public static List<Contract__c> joinContracts(Contract__c sourceContract, List<wrapPointsContract> relatedContracts) {
		List<Contract__c> contractsToUpdate = new List<Contract__c> ();
		contractsToUpdate.add(sourceContract);
		if (relatedContracts != null) {
			for (wrapPointsContract relatedContractWrapper : relatedContracts) {
				if (relatedContractWrapper.selected) {
					contractsToUpdate.add(relatedContractWrapper.contract);
				}
			}
		}
		return contractsToUpdate;
	}
	public static boolean isOwnerDirty(Owner__c originalOwner, Owner__c editedOwner) {
		boolean retVal = originalOwner.Salutation__c != editedOwner.Salutation__c ||
		originalOwner.First_Name__c != editedOwner.First_Name__c ||
		originalOwner.Middle_Name__c != editedOwner.Middle_Name__c ||
		originalOwner.Last_Name__c != editedOwner.Last_Name__c ||
		originalOwner.Suffix__c != editedOwner.Suffix__c ||
		originalOwner.Trust__c != editedOwner.Trust__c ||
		originalOwner.Street__c != editedOwner.Street__c ||
		originalOwner.City__c != editedOwner.City__c ||
		originalOwner.State__c != editedOwner.State__c ||
		originalOwner.Postal_Code__c != editedOwner.Postal_Code__c ||
		originalOwner.Country__c != editedOwner.Country__c;
		return retVal;
	}

	public static Integer countDirtyOwners(List<Owner__c> dirtyOwnerList, List<Owner__c> cleanOwnerList) {
		Integer retVal = 0;
		for (Owner__c o1 : dirtyOwnerList)
		{
			for (Owner__c o2 : cleanOwnerList)
			{
				if (o1.Id == o2.Id) {
					if (isOwnerDirty(o1, o2)) {
						retVal++;
					}
					break;
				}
			}
		}
		return retval;
	}

	public static Map<String, Object> loadData(List<Points_Batch_Closing__c> dataRows, List<Points_Batch_Detail_Owner__c> ownerRows, string batchType, string batchId, string clubId) {
		Map<String, Object> retMap = new Map<String, Object> ();
		retMap.put('errors', new Map<String, Object> ());
		retMap.put('records', new Map<String, Object> ());

		// UOW setup
		List<SObjectType> uowObjs = new List<SObjectType> {
			Points_Batch_Header__c.SObjectType,
			Points_Batch_Closing__c.SObjectType,
			Points_Batch_Detail_Owner__c.SObjectType,
			Membership_Number__c.SObjectType,
			Contract__c.SObjectType,
			Transaction__c.SObjectType,
			Owner__c.SObjectType,
			Owner_List__c.SObjectType
		};

		fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(uowObjs);

		// Batch Header
		Id batchHeaderRecordType = sObjectType.Points_Batch_Header__c.RecordTypeInfosByName.get(batchType).RecordTypeId;
		Id batchRecordType = sObjectType.Points_Batch_Closing__c.RecordTypeInfosByName.get(batchType).RecordTypeId;

		Points_Batch_Header__c pbh = new Points_Batch_Header__c();
		pbh.RecordTypeId = batchHeaderRecordType;
		pbh.Name = batchId;
		pbh.Points_Id__c = batchId;
		pbh.Batch_Date__c = Date.today();
		pbh.Employee__c = UserInfo.getUserId();
		pbh.Developer__c = clubId;
		uow.registerNew(pbh);

		// Batch Details 
		for (Points_Batch_Closing__c bc : dataRows) {
			bc.RecordTypeId = batchRecordType;
			uow.registerNew(bc, Points_Batch_Closing__c.Points_Batch_Header__c, pbh);
			contractOwners cOwners = new contractOwners();
			for (Points_Batch_Detail_Owner__c pbdo : ownerRows) {
				if (pbdo.Contract_Number__c == bc.Contract_Number__c) {
					uow.registerNew(pbdo, Points_Batch_Detail_Owner__c.Points_Batch_Detail__c, bc);
					ownerName oName = new ownerName(pbdo.Title__c, pbdo.First_Name__c, pbdo.Middle_Name__c, pbdo.Last_Name__c, pbdo.Suffix__c);
					cOwners.addOwner(oName);
					if (pbdo.Ordinal__c == 1) {
						bc.Name_1_Salutation__c = oName.title;
						bc.Name_1_First__c = oName.first;
						bc.Name_1_Middle__c = oName.middle;
						bc.Name_1_Last__c = oName.last;
						bc.Name_1_Suffix__c = oName.suffix;
						bc.Name_1__c = oName.fullName;
					}
					if (pbdo.Ordinal__c == 2) {
						bc.Name_2_Salutation__c = oName.title;
						bc.Name_2_First__c = oName.first;
						bc.Name_2_Middle__c = oName.middle;
						bc.Name_2_Last__c = oName.last;
						bc.Name_2_Suffix__c = oName.suffix;
						bc.Name_2__c = oName.fullName;
					}
					if (pbdo.Ordinal__c == 3) {
						bc.Name_3__c = oName.fullName;
					}
					if (pbdo.Ordinal__c == 4) {
						bc.Name_4__c = oName.fullName;
					}
					if (pbdo.Ordinal__c == 5) {
						bc.Name_5__c = oName.fullName;
					}
					if (pbdo.Ordinal__c == 6) {
						bc.Name_6__c = oName.fullName;
					}
				}
			}
			bc.Owners__c = cOwners.contractOwners;
		}
		insertContracts(batchType, dataRows, ownerRows, batchId, clubId, retMap, uow);
		try {
			// UOW commit
			uow.commitWork();
			// Add new Ids to return variable
			((Map<String, Object>) retMap.get('records')).put('Points_Batch_Header__c', pbh.Id);

		} catch(Exception e) {
			((Map<String, Object>) retMap.get('errors')).put('Points_Batch_Header__c', e.getMessage());
			return retMap;

		}

		return retMap;
	}

	public static void insertContracts(string batchType, List<Points_Batch_Closing__c> dataRows, List<Points_Batch_Detail_Owner__c> ownerRows, string batchId, string clubId, Map<String, Object> retMap, fflib_SObjectUnitOfWork uow) {

		Map<String, String> seasonMap = new Map<String, String> ();
		if (batchType == 'Welk') {
			seasonMap = new Map<String, String> { 'A' => 'Annual', 'O' => 'Odd', 'E' => 'Even' };
		}

		// Member number can already exist, so don't add a duplicate
		Set<String> mnNumbers = new Set<String> ();
		for (Points_Batch_Closing__c bc : dataRows) {
			mnNumbers.add(bc.Owner_Number__c);
		}
		// Query the existing Membership Numbers from SF
		List<Membership_Number__c> mnExisting = [
		                                         select Id, Name
		                                         from Membership_Number__c
		                                         WHERE Name in :mnNumbers
		                                         and Developer__c = :clubId
		                                        ];
		// Build a map of the Membership Numbers
		Map<string, Membership_Number__c> mnExistingMap = new Map<string, Membership_Number__c> ();
		for (Membership_Number__c mn : mnExisting) {
			mnExistingMap.put(mn.Name, mn);
		}

		for (Points_Batch_Closing__c bc : dataRows) {
			// If the row was not validated successfully, do not load the transactions
			if (bc.Verified__c == false) { continue; }

			// Membership Number - if already exists, no need to register new
			Membership_Number__c mn;
			if (mnExistingMap.containsKey(bc.Owner_Number__c)) {
				mn = mnExistingMap.get(bc.Owner_Number__c);
			} else {
				mn = new Membership_Number__c(Name = bc.Owner_Number__c, Points_Id__c = bc.Name, Developer__c = clubId);
				uow.registerNew(mn, Membership_Number__c.Points_Batch_Closing__c, bc);
				incrementReturnRecordCount('Membership_Number__c', retMap);
			}

			// Contract
			Contract__c contract = newContract(batchType, bc, seasonMap);
			uow.registerNew(contract, Contract__c.Points_Batch_Closing__c, bc);
			uow.registerRelationship(contract, Contract__c.Membership_Number__c, mn);
			incrementReturnRecordCount('Contract__c', retMap);

			// Transaction
			Transaction__c trans = new Transaction__c();
			trans.Points_Id__c = bc.Name;
			trans.Transaction_Type__c = bc.Transaction_Type_Id__c;
			trans.Closing_Date__c = bc.Closing_Date__c; // closing date from picklist
			trans.Entered__c = Date.today();
			trans.Employee__c = UserInfo.getUserId();
			uow.registerNew(trans, Transaction__c.Points_Batch_Closing__c, bc);
			uow.registerRelationship(trans, Transaction__c.ContractId__c, contract);
			incrementReturnRecordCount('Transaction__c', retMap);

			// Owners : There can be more than one owner per incoming record
			integer nameOrdinal = 0;
			for (Points_Batch_Detail_Owner__c ownerRow : ownerRows) {
				nameOrdinal++;
				if (bc.Contract_Number__c == ownerRow.Contract_Number__c) {
					string ownerName = ownerRow.Full_Name__c;
					if (ownerName != '') {
						Owner__c newOwner = newOwner(batchType, bc, ownerRow, nameOrdinal);
						uow.registerNew(newOwner, Owner__c.Points_Batch_Closing__c, bc);
						incrementReturnRecordCount('Owner__c', retMap);

						Owner_List__c ol = new Owner_List__c();
						ol.Points_Id__c = bc.Name + '|' + nameOrdinal;
						ol.Points_Batch_Closing__c = bc.Id;
						uow.registerNew(ol, Owner_List__c.Points_Batch_Closing__c, bc);
						uow.registerRelationship(ol, Owner_List__c.Owner__c, newOwner);
						uow.registerRelationship(ol, Owner_List__c.Transaction__c, trans);
						incrementReturnRecordCount('Owner_List__c', retMap);
					}
				}
			}

		}

	}
	public static Contract__c newContract(string batchType, Points_Batch_Closing__c bc, Map<String, String> seasonMap) {
		Contract__c contract = new Contract__c();
		contract.Points_Id__c = bc.Name;
		contract.Date_Signed__c = bc.Contract_Date__c;
		contract.Name = bc.Contract_Number__c;
		contract.Batch_Number__c = String.valueOf(bc.Batch__c);
		contract.Resort__c = bc.Resort__c;
		contract.Lender_Id__c = bc.Lender_Id__c;
		contract.Member_Type__c = bc.Member_Type_Id__c;
		contract.Owners__c = bc.Owners__c;
		if (batchType == 'Shell') {
			contract.Points__c = bc.Convert_Points__c + bc.New_Points__c;
			contract.Expiration_Date__c = bc.Expiration_Date_Id__c; // 
			contract.Membership_Type__c = bc.Membership_Type_Id__c; //
		}
		if (batchType == 'Welk') {
			contract.Points__c = bc.Sum_Points__c;
			if (bc.Season__c == 'A' || bc.Season__c == 'O' || bc.Season__c == 'E') {
				contract.Season__c = seasonMap.get(bc.Season__c);
			}

		}
		return contract;
	}

	public static Owner__c newOwner(string batchType, Points_Batch_Closing__c bc, Points_Batch_Detail_Owner__c ownerRow, integer nameOrdinal) {
		Owner__c newOwner = new Owner__c();
		newOwner.Points_Id__c = bc.Name + '|' + nameOrdinal;
		newOwner.First_Name__c = ownerRow.First_Name__c;
		newOwner.Last_Name__c = ownerRow.Last_Name__c;
		newOwner.Street__c = bc.Owner_Address__c;
		newOwner.City__c = bc.Owner_City__c;
		newOwner.State__c = bc.Owner_State__c;
		newOwner.Postal_Code__c = bc.Owner_Postal_Code__c;
		newOwner.Country__c = bc.Owner_Country__c;
		newOwner.Trust__c = bc.Trust__c == '' ? null : bc.Trust__c;
		if (batchType == 'Shell') {
			// any shell-specific owner fields here
		}
		if (batchType == 'Welk') {

			newOwner.Salutation__c = ownerRow.Title__c;
			newOwner.Middle_Name__c = ownerRow.Middle_Name__c;
			newOwner.Suffix__c = ownerRow.Suffix__c;
		}
		return newOwner;
	}
	public static void incrementReturnRecordCount(string objectName, Map<String, Object> retMap) {
		if (!((Map<String, Object>) retMap.get('records')).containsKey(objectName)) {
			((Map<String, Object>) retMap.get('records')).put(objectName, 1);
		} else {
			((Map<String, Object>) retMap.get('records')).put(objectName, ((Integer) ((Map<String, Object>) retMap.get('records')).get(objectName)) + 1);
		}
	}
	public class ownerName {
		public string title { get; set; }
		public string first { get; set; }
		public string middle { get; set; }
		public string last { get; set; }
		public string suffix { get; set; }
		private string pFullname;
		public string fullName {
			get {
				string retVal = pFullname == null ? (clean(title) + clean(first) + clean(middle) + clean(last) + clean(suffix)).trim() : pFullname;
				return retVal;
			}
		}

		public ownerName(string t, string f, string m, string l, string s) {
			this.title = t;
			this.first = f;
			this.middle = m;
			this.last = l;
			this.suffix = s;
		}
		public ownerName(string full) {
			this.pFullname = full;
		}
		private string clean(string s) {
			return string.isBlank(s) ? '' : s + ' ';
		}
	}

	public class contractOwners {
		public List<ownerName> ownerNames { get; set; }
		public string trustName { get; set; }
		public string contractOwners {
			get {
				if (!string.isBlank(trustName)) {
					return trustName;
				}
				string retVal = '';
				if (ownerNames.size() == 1) {
					retVal = ownerNames[0].FullName;
				}
				if (ownerNames.size() > 1) {
					retVal = ownerNames[0].FullName;
					for (integer i = 1; i<ownerNames.size() - 1; i++) {
						retVal = retVal + ', ' + ownerNames[i].FullName;
					}
					retVal = retVal + ' and ' + ownerNames[ownerNames.size() - 1].FullName;
				}
				return retVal;
			}
		}

		public contractOwners() {
			ownerNames = new List<ownerName> ();
		}
		public contractOwners(string ownerNameString) {
			ownerNames = new List<ownerName> ();
			addOwner(ownerNameString);
		}
		public contractOwners(List<ownerName> ownerNames) {
			this.ownerNames = ownerNames;
		}
		public contractOwners(ownerName ownerName) {
			this.ownerNames = new List<ownerName> { ownerName };
		}
		public contractOwners(List<ownerName> ownerNames, string trustName) {
			this.ownerNames = ownerNames;
			this.trustName = trustName;
		}

		public void addOwner(string ownerNameString) {
			ownerName o = new ownerName(ownerNameString);
			ownerNames.add(o);
		}
		public void addOwner(ownerName o) {
			ownerNames.add(o);
		}

	}

}