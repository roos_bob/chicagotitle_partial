public with sharing class Deeded_DocumentSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Deeded_Document__c.Id,
			Deeded_Document__c.Name

		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Deeded_Document__c.sObjectType;
	}

	public List<Deeded_Document__c> selectById(Set<ID> idSet)
	{
		return (List<Deeded_Document__c>) selectSObjectsById(idSet);
	}

	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Deeded_Document__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Deeded_Document__c>) Database.query(query.toSOQL());
	}

	public List<Deeded_Document__c> selectByIdAndHasDocType(Set<Id> idSet)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Id IN :idSet and Deeded_Document_Type__c != null');
		query.selectFields(new Set<String> {'Deeded_Document_Type__r.Name', 'Deeded_Document_Type__c', 'Deeded_Document_Type__r.Is_Deed_of_Trust__c', 'Deeded_Document_Type__r.Is_Developer_Grant_Deed__c', 'Deeded_Document_Type__r.Is_Reconveyance__c', 'Deeded_Document_Type__r.Is_Trustees_Deed__c'});
		return  (List<Deeded_Document__c>) Database.query(query.toSOQL());
	}
	

}