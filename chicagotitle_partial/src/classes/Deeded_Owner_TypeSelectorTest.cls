@isTest
private class Deeded_Owner_TypeSelectorTest {

	@isTest
	private static void testSelector() {
		Deeded_Owner_Type__c record = new Deeded_Owner_Type__c(Name = 'Individual', Is_Batch_Import_Default__c=true, Owner_Type__c='5');
		insert record;

		Deeded_Owner_TypeSelector selector = new Deeded_Owner_TypeSelector();
		Set<Id> idSet = new Set<Id> {record.Id};
		List<String> nameList = new List<String> {'Individual'};
		String nameString = 'Individual';
		
		List<Deeded_Owner_Type__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Deeded_Owner_Type__c> byNameList = selector.selectByName(nameList);
		System.assert(byNameList.size() == 1);

		List<Deeded_Owner_Type__c> byName = selector.selectByName(nameString);
		System.assert(byName.size() == 1);
		
		Id defaultId = selector.selectDefault();
		System.assert(defaultId != null);

	}
}