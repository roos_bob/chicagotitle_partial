@isTest
private class Tag_SheetSelectorTest {

	@isTest
	private static void testSelector() {
		Tag_Sheet__c record = new Tag_Sheet__c();
		insert record;
		Tag_Sheet__c ts = [select id, name from Tag_Sheet__c where id = :record.Id];

		Set<Id> idSet = new Set<Id> {ts.Id};
		List<String> nameList = new List<String> {ts.Name};
		//Set<String> nameSet = new Set<String> {ts.Name};
		
		Tag_SheetSelector selector = new Tag_SheetSelector();

		List<Tag_Sheet__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Tag_Sheet__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 1);
		
		
		Id idByBatchName = selector.selectIdByBatchName('test');
		System.assert(idByBatchName == null);

	}
}