/*
Test Class: Contract_BatchCancel_Test
*/
global class Contract_BatchCancel implements Database.Batchable<SObject>, Database.Stateful {
	public List<String> contractNames;
	public List<String> recipients;
	public Id developerId;
	public Date closingDate;
	public string comments;
	public Map<String, Boolean> contractFound;

	global Contract_BatchCancel(List<string> contractNames, Id developerId, Date closingDate, String comments, List<String> userEmail) {
		this.contractNames = contractNames;
		this.developerId = developerId;
		this.closingDate = closingDate;
		this.comments = comments;
		this.recipients = userEmail;
		contractFound = new Map<String, Boolean> ();
		for (string contractName : contractNames) {
			contractFound.put(contractName, false);
		}
	}

	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */
	global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator('SELECT Id, Name FROM Contract__c where Name in :contractNames and Resort__r.Developer__c = :developerId');
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */
	global void execute(Database.BatchableContext context, List<Contract__c> scope) {
		for (Contract__c contract : scope) {
			contractFound.put(contract.Name, true);
			Contract_Helper.newCancelTransaction(Contract.Id, closingDate, comments, new List<wrapPointsContract>());
		}
	}

	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */
	global void finish(Database.BatchableContext context) {
		String developerName = [SELECT Name FROM Developer__c WHERE Id = :developerId].Name;
		String subject = String.format('Points Contract Batch Cancel for Inventory for {0} is complete.', new String[] { developerName });
		String body = String.format('Points Contract Batch Cancel for {0} is complete.', new String[] { developerName });
		body = body + '<br />';
		body = body + 'Contracts cancelled: <br />';
		for (string key : contractFound.keySet()) {
			if (contractFound.get(key)) {
				body = body + key + '<br />';
			}
		}
		body = '<br /><br />' + body + 'Contracts not found: <br />';
		for (string key : contractFound.keySet()) {
			if (!contractFound.get(key)) {
				body = body + key + '<br />';
			}
		}
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage> ();

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(recipients);
		mail.setSubject(subject);
		mail.setHtmlBody(body);

		mails.add(mail);

		Messaging.sendEmail(mails);
		
	}
}