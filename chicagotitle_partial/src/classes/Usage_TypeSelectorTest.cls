@isTest
private class Usage_TypeSelectorTest {

	@isTest
	private static void testSelector() {

		Usage_Type__c record = new Usage_Type__c(Name = 'test');
		insert record;

		Usage_TypeSelector selector = new Usage_TypeSelector();
		Set<Id> idSet = new Set<Id> {record.Id};
		List<String> nameList = new List<String> {'test'};
		Set<String> nameSet = new Set<String> {'test'};
		Set<String> codeSet = new Set<String>();
		
		List<Usage_Type__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Usage_Type__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 1);
		
		List<Usage_Type__c> byNameSet = selector.selectByName(nameSet);
		System.assert(byNameSet.size() == 1);
		
		List<Usage_Type__c> byCodeSet = selector.selectByCode(codeSet);
		System.assert(byCodeSet.size() == 0);
		
		Map<String, Usage_Type__c> mapByName = selector.selectMapByCode(codeSet);
		System.assert(mapByName.size() == 0);

	}
}