public with sharing class Resort_OwnershipSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Resort_Ownership__c.Id,
			Resort_Ownership__c.Name,
			Resort_Ownership__c.ResortId__c,
			Resort_Ownership__c.Ownership_Code__c,
			Resort_Ownership__c.Developer_Owned__c
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Resort_Ownership__c.sObjectType;
	}

	public List<Resort_Ownership__c> selectById(Set<ID> idSet)
	{
		return (List<Resort_Ownership__c>) selectSObjectsById(idSet);
	}

	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	//public List<Resort_Ownership__c> selectByName(List<String> nameList)
	//{
		//fflib_QueryFactory query = initNewQueryFactory();
		//query.setCondition('Name IN :nameList');
		//return  (List<Resort_Ownership__c>) Database.query(query.toSOQL());
	//}

	public Map<String, Resort_Ownership__c> selectMapByCode(Set<String> codes)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Ownership_Code__c IN :codes');
		List<Resort_Ownership__c> records = (List<Resort_Ownership__c>) Database.query(query.toSOQL());

		Map<String, Resort_Ownership__c> resultMap = new Map<String, Resort_Ownership__c>();
		for (Resort_Ownership__c record: records) {
			resultMap.put(record.Ownership_Code__c, record);
		}
		return resultMap;

	}

}