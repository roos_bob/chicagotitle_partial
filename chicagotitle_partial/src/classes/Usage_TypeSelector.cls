public with sharing class Usage_TypeSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
			Usage_Type__c.Id,
			Usage_Type__c.Name,
			Usage_Type__c.Resort__c,
			Usage_Type__c.Code__c,
			Usage_Type__c.ICN_Code__c
		};
	}
	
	public Schema.SObjectType getSObjectType()
	{
		return Usage_Type__c.sObjectType;
	}

	public List<Usage_Type__c> selectById(Set<ID> idSet)
	{
		return (List<Usage_Type__c>) selectSObjectsById(idSet);
	}
	
	public fflib_QueryFactory initNewQueryFactory() {
		fflib_QueryFactory query = newQueryFactory();
		return query;
	}

	public List<Usage_Type__c> selectByName(List<String> nameList)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameList');
		return  (List<Usage_Type__c>) Database.query(query.toSOQL());
	}

	public List<Usage_Type__c> selectByName(Set<String> nameSet)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Name IN :nameSet');
		return  (List<Usage_Type__c>) Database.query(query.toSOQL());
	}

	public List<Usage_Type__c> selectByCode(Set<String> codes)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Code__c IN :codes');
		return  (List<Usage_Type__c>) Database.query(query.toSOQL());
	}

	public Map<String, Usage_Type__c> selectMapByCode(Set<String> codes)
	{
		fflib_QueryFactory query = initNewQueryFactory();
		query.setCondition('Code__c IN :codes');
		List<Usage_Type__c> records = (List<Usage_Type__c>) Database.query(query.toSOQL());
		Map<String, Usage_Type__c> resultMap = new Map<String, Usage_Type__c>();
		for (Usage_Type__c record: records) {
			resultMap.put(record.Code__c, record);
		}
		return resultMap;
	}


}