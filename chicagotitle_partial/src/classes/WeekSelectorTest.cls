@isTest
private class WeekSelectorTest {

	@isTest
	private static void testSelector() {

		Week__c record = new Week__c(Name = 'test');
		insert record;

		WeekSelector selector = new WeekSelector();
		Set<Id> idSet = new Set<Id> {record.Id};
		List<String> nameList = new List<String> {'test'};
		Set<String> nameSet = new Set<String> {'test'};
		Set<String> codeSet = new Set<String>();
		List<String> weekList = new List<String>();
		
		
		List<Week__c> byID = selector.selectById(idSet);
		System.assert(byId.size() == 1);

		List<Week__c> byName = selector.selectByName(nameList);
		System.assert(byName.size() == 1);
		

		List<Week__c> byNameId = selector.selectByNameAndResortId(weekList, null);
		List<Week__c> byResortWeek = selector.selectByResortNameAndWeekName(null, weekList );
		
		Map<String, Week__c> mapByName = selector.selectMapByCode(codeSet);
		System.assert(mapByName.size() == 0);



	}
}